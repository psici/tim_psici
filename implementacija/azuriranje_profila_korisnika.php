<html> 
<head>
	<meta charset="UTF-8">
	<title> Azuriranje profila korisnika </title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body> 
	<div id="page">

	<?php
		require_once('meni_korisnik.php'); 
	?>

	<div class="body">
		<br/>
		<h3 align="center">
			<font color = "#800000"> 
				Informacije o korisniku
			</font>
		</h3>
		<br/>
		<form action="index.html" method="post" id="message" class="section">
			 <table name="updateuser" width="60%" > 
				<tr>
					<td align="left">
					<label align="left"><font color="black" align="left"> Ime : </font></label>
					</td>
					<td colspan = "2">
						<input type="text" name="name" length="20"/>
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
						<label align="left"><font color="black" align="left"> Iskustva : </font></label>
					</td>
					<td>
						
					</td>
				</tr>
				
				<tr>
					<td align="left">
						<label align="left"><font color="black" align="left"> Prezime : </font></label>
					</td>
					<td colspan = "2">
						<input type="text" name="surname" length="20"/>
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td colspan="2" rowspan="3">
						<textarea name="expirience" cols = "55" rows = "5"> </textarea>
					</td>
				</tr>
				
				<tr>
					<td align="left">
						<label align="left"><font color="black" align="left"> Stara lozinka : </font></label>
					</td>
					<td colspan = "2">
						<input type="password" name="passold" length="20"/>
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					
				</tr>
				
				<tr>
					<td align="left">
					<label align="left"><font color="black" align="left"> Nova lozinka : </font></label>
					</td>
					<td colspan = "2">
						<input type="password" name="passnew" length="20"/>
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					
				</tr>
				
				<tr>
					<td align="left">
						<label align="left"><font color="black" align="left"> Lozinka opet : </font></label>
					</td>
					<td colspan = "2">
						<input type="password" name="passnewagain" length="20"/>
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
					<label align="left"><font color="black" align="left"> Znanja : </font></label>
					</td>
					<td>
						
					</td>
				</tr>
				
				<tr>
					<td align="left">
						<label align="left"><font color="black" align="left"> E-mail : </font></label>
					</td>
					<td colspan = "2">
						<input type="text" name="email" length="20"/>
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td colspan="2" rowspan="3">
						<textarea name="knowledge" cols = "55" rows = "5"> </textarea>
					</td>
				</tr>
				
				<tr>
					<td align="left">
						<label align="left"><font color="black" align="left"> Pol : </font></label>
					</td>
					<td colspan = "2">
						<select name="gender">
						  <option value="none">&nbsp;</option>
						  <option value="male">Muski</option>
						  <option value="female">Zenski</option>
						</select> 
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
				
				<tr>
					<td align="left">
					<label align="left"><font color="black" align="left"> Sajt : </font></label>
						</td>
					<td colspan = "2">
						<input type="text" name="site" length="20"/>
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>

				<tr>
					<td align="left">
						<label align="left"><font color="black" align="left"> Fakultet : </font></label>
					</td>
					<td colspan = "2">
						<input type="text" name="faculty" length="20"/>
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
					<label align="left"><font color="black" align="left"> Projekti </font></label>
					</td>
					<td>
						
					</td>
				</tr>
				
				<tr>
					<td align="left">
						<label align="left"><font color="black" align="left"> Smer : </font></label>
					</td>
					<td colspan = "2">
						<input type="text" name="department" length="20"/>
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
						<label align="left"><font color="black" align="left"> Opis : </font></label>
					</td>
					<td>
						<input type="text" name="desc" length="20" style="width: 200px;"/>
					</td>
				</tr>
				
				<tr>
					<td align="left">
						<label align="left"><font color="black" align="left"> CV : </font></label>
					</td>
					<td>
						<input type="text" name="cv" length="20" style="width: 200px;"/>
					</td>
					<td>
						<input type="button" value="Pretrazi" />
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
						<label align="left"><font color="black" align="left"> URL : </font></label>
					</td>
					<td>
						<input type="text" name="email" length="20" style="width: 200px;"/> &nbsp;&nbsp;&nbsp;<input type="button" value="Dodaj"/>
					</td>
				</tr>
				
				<tr>
					<td align="left">
						<label align="left"><font color="black" align="left"> URL slike : </font></label>
					</td>
					<td>
						<input type="text" name="picurl" length="20" style="width: 200px;"/>
					</td>
					<td>
						<input type="button" value="Pretrazi"/>
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
					<label align="left"><font color="black" align="left"> Nagrade : </font></label>
					</td>
					<td>
						<input type="text" name="awards" length="20"/>
					</td>
				</tr>
				
				<tr>
					<td align="left">
					<label align="left"><font color="black" align="left"> </font></label>
					</td>
					<td>
						
					</td>
					<td>
						
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
					
					</td>
					<td>
						
					</td>
				</tr>			
				<tr>
					<td align="left">
					
					</td>
					<td>
						<input type="button" value="Potvrdi"/> &nbsp; &nbsp;<input type="button" value="Ponisti"/>
					</td>
					<td>
						
					</td>
					<td width="3%">
						
					</td>
					<td>
						
					</td>
					<td>
						<a href=""> Deaktiviraj profil </font></a>
					</td>
				</tr>
								
				<tr>
					<td align="left">
					
					</td>
					</td>
					<td>
						
					</td>
					<td>
						
					</td>
					<td width="3%">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
					
					</td>
					<td>
			
					</td>
				</tr>
			 </table>
	   </form>
	</div>

	<?php
		require_once('footer.php'); 
    ?>

	</div>
</body>
</html>
