CREATE TABLE Comment
(
	Id                   INTEGER NOT NULL,
	Description          VARCHAR(255) NULL,
	UserName             INTEGER NOT NULL,
	Company              INTEGER NOT NULL,
	thumbs_up            INTEGER NULL,
	thumbs_down          INTEGER NULL
);

ALTER TABLE Comment
ADD PRIMARY KEY (Id);

CREATE TABLE Company
(
	Username             VARCHAR(30) NOT NULL,
	Password             VARCHAR(16) NULL,
	E_mail               VARCHAR(40) NULL,
	Sector               VARCHAR(20) NULL,
	Site                 VARCHAR(30) NULL,
	Description          VARCHAR(255) NULL,
	NumOfEmployees       INTEGER NULL,
	FoundationYear       INTEGER NULL,
	FieldOfWork          VARCHAR(100) NULL,
	Headquarter          VARCHAR(30) NULL,
	AverageGrade         DECIMAL(5,2) NULL,
	GradeSum             DECIMAL(10,0) NULL,
	GradeNumber          INTEGER NULL
);

ALTER TABLE Company
ADD PRIMARY KEY (Username);

CREATE TABLE Offer
(
	Id                   INTEGER NOT NULL,
	Type                 CHAR(1) NULL,
	Position             VARCHAR(40) NULL,
	Experience           VARCHAR(255) NULL,
	EmploymentType       CHAR(1) NULL,
	StartDate            CHAR(10) NULL,
	EndDate              CHAR(10) NULL,
	Qualifications       VARCHAR(255) NULL,
	Preffered            VARCHAR(255) NULL,
	Description          VARCHAR(255) NULL,
	ExpireDate           CHAR(10) NULL,
	Username             VARCHAR(30) NOT NULL
);

ALTER TABLE Offer
ADD PRIMARY KEY (Id);

CREATE TABLE Project
(
	Id                   INTEGER NOT NULL,
	Description          VARCHAR(255) NULL,
	URL                  VARCHAR(40) NULL,
	Usename              VARCHAR(30) NOT NULL
);

ALTER TABLE Project
ADD PRIMARY KEY (Id);

CREATE TABLE Question
(
	Id                   INTEGER NOT NULL,
	QText                VARCHAR(255) NULL,
	Answer               VARCHAR(255) NULL,
	QType                CHAR(1) NULL
);

ALTER TABLE Question
ADD PRIMARY KEY (Id);

CREATE TABLE Telephone_Company
(
	Number               VARCHAR(20) NOT NULL,
	Username             VARCHAR(30) NOT NULL
);

ALTER TABLE Telephone_Company
ADD PRIMARY KEY (Number,Username);

CREATE TABLE User
(
	Usename              VARCHAR(30) NOT NULL,
	FirstName            VARCHAR(30) NULL,
	LastName             VARCHAR(30) NULL,
	Gender               CHAR(1) NULL,
	Site                 VARCHAR(50) NULL,
	Faculty              VARCHAR(50) NULL,
	Department           VARCHAR(50) NULL,
	Experience           VARCHAR(255) NULL,
	Skills               VARCHAR(255) NULL,
	Awards               VARCHAR(255) NULL,
	IsAdmin              boolean NULL,
	Points               INTEGER NULL,
	Email                CHAR(18) NULL,
	Pass                 CHAR(18) NULL,
	Birthdate            VARCHAR(10) NULL,
	Rank             INTEGER NULL,
	PathPicture          VARCHAR(255) NULL,
	PathCV               VARCHAR(255) NULL
);

ALTER TABLE User
ADD PRIMARY KEY (Usename);

ALTER TABLE Comment
ADD FOREIGN KEY R_6 (UserName) REFERENCES User (Usename);

ALTER TABLE Comment
ADD FOREIGN KEY R_7 (Company) REFERENCES Company (Username);

ALTER TABLE Offer
ADD FOREIGN KEY R_2 (Username) REFERENCES Company (Username);

ALTER TABLE Project
ADD FOREIGN KEY R_1 (Usename) REFERENCES User (Usename);

ALTER TABLE Telephone_Company
ADD FOREIGN KEY R_4 (Username) REFERENCES Company (Username)
		ON DELETE CASCADE;
