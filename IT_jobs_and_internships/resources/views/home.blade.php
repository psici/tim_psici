@extends('master')
@section('menusection')
    @include('shared.not_connected')
@endsection
@section('homesection')
<div class="jumbotron feature" style="...">
    <div class="container">
        <h1><span class="glyphicon glyphicon-equalizer"></span> IT poslovi i prakse</h1>
        <p>Budite uvek u toku!</p>
    </div>
</div>
@endsection

@section('title', 'Home')

@section('content')

    <form class="form-horizontal" action="{{route('signin')}}" method="post" name="homeForm" id="homeForm">
        <fieldset>

            <!-- Text input-->
            <div class="form-group {{ $errors->has('uname') ? 'has-error' : '' }}">
                <label class="col-md-4 control-label" for="uname" style="color: #2F3133;">Korisnicko ime:</label>
                <div class="col-md-4">
                    <input id="uname" name="uname" type="text" placeholder="Unesite korisnicko ime" class="form-control input-md" required="" value="{{ Request::old('uname') }}">
                    <div class ="text-danger">  {{ $errors->first('uname') }}</div>
                </div>
            </div>

            <!-- Password input-->
            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <label class="col-md-4 control-label" for="pass" style="color: #2F3133;">Lozinka:</label>
                <div class="col-md-4">
                    <input id="pass" name="password" type="password" placeholder="•••••••••••••" class="form-control input-md" required="">
                    <div class ="text-danger">  {{ $errors->first('password') }}</div>
                </div>
            </div>

            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="userType" style="color: #2F3133;">Tip:</label>
                <div class="col-md-4">
                    <select id="userType" name="userType" class="form-control" onchange="document.getElementById('selected_text').value=this.options[this.selectedIndex].text">
                        <option value="regularUser">Korisnik</option>
                        <option value="company">Firma</option>
                        {{--<option value="admin">Administrator</option>--}}{{-- Ne treba nam ovo, admin je korisnik i znacemo po polju iz baze da je admin--}}
                    </select>
                    <input type="hidden" name="selected_text" id="selected_text" value="Korisnik" />
                </div>
            </div>

            <div id="success" name="success">
                @if($errors->any())
                    <center><font color="red">{{$errors->first()}}</font></center>
                @endif
            </div>

            <!-- Button -->
            <div class="form-group" align="center">
                <label class="col-md-4 control-label" for="login"></label>
                <div class="col-md-4">
                    <button id="login" name="login" class="btn btn-success">Prijavi se</button>
                </div>
            </div>
            <div align="center">
                <p>Jos uvek nemate profil?</p>
                <a href="{{ route('registerUser') }}"><u><i>Registracija korisnika</i></u></a> |
                <a href="{{ route('registerCompany') }}"><u><i>Registracija firme</i></u></a>
            </div>
        </fieldset>
        <input type="hidden" name="_token" value="{{Session::token()}}"/>
        <br/><br/>
    </form>

@endsection