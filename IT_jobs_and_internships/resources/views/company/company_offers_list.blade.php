@section('homesection')
    <img style="width:100%;height:24%;background:#80944c" src="{{asset('images/profil_cmp_4.jpg')}}" alt="sfafsa">
@endsection

@extends('master')
@section('title', 'Ponude firme')
@section('menusection')
    <?php
    if (session_status() == PHP_SESSION_NONE)
    session_start();
    ?>
    @include('shared.'.$_SESSION['Header'])
@endsection
@section('content')

    <?php
    if (!(is_null($Offers))){
    foreach($Offers as $offer){
    ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                <br/>
                <div class="panel panel-info">
                    <div class="panel-heading" style="background-color:#5e5d5d">
                        <h3 class="panel-title"> <font color="white"> Informacije o ponudi </font></h3>
                    </div>

                    <?php
                        $tip = $offer->Type;
                        $slika ="";
                        if ($tip == "I"){
                            $tip = "Praksa";
                            $slika = "images/internlogo.jpg";
                            }
                        else
                            {
                            $tip = "Posao";
                            $slika = "images/joblogo.jpg";
                            }

                        $employment = $offer->EmploymentType;
                        if ($employment ="F")
                            $employment ="Full Time pozicija";
                        else
                            $employment ="Part Time pozicija";
                    ?>

                    <input type="hidden" id="jsfirma" name="jsfirma" value="#"/>
                    <div class="panel-body">
                        <div class="row" >
                            <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="{{asset($slika)}}" class="img-circle img-responsive"> </div>

                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>Tip :</td>
                                        <td> <?php echo $tip ?> </td>
                                    </tr>
                                    <tr>
                                        <td>Pozicija:</td>
                                        <td><?php echo $offer->Position ?></td>
                                    </tr>
                                    <tr>
                                        <td> Potrebno radno iskustvo  :</td>
                                        <td><?php echo $offer->Experience ?> godina iskustva.
                                    </tr>
                                    <tr>
                                    <tr>
                                        <td>Radno vreme :</td>
                                        <td><?php echo $employment ?></td>
                                    </tr>
                                    <tr>
                                        <td>Datum pocetka :</td>
                                        <td> <?php echo $offer->StartDate ?></td>
                                    </tr>
                                    <tr>
                                        <td>Datum kraja : </td>
                                        <td> <?php echo $offer->EndDate ?></td>
                                    </tr>
                                    <tr>
                                        <td> Obavezne kvalifikacije : </td>
                                        <td> <?php echo $offer->Qualifications ?> </td>

                                    </tr>
                                    <tr>
                                        <td> Pozeljne kvalifikacije : </td>
                                        <td> <?php echo $offer->Preffered ?> </td>
                                    </tr>
                                    <tr>
                                        <td> Opis </td>
                                        <td> <?php echo $offer->Description?>  </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <a href="<?php echo 'mailto:'.$Email ?>" class="btn btn-primary"> Kontaktiraj firmu za datu ponudu </a>

                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
        <?php } } ?>

    <br/>
    <br/>
    <br/>

@endsection