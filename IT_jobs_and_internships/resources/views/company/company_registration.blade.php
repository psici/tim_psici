@section('homesection')
  <img style="width:100%;height:24%;background:#80944c" src="{{asset('images/companycover.jpg')}}" alt="sfafsa">
@endsection

@extends('master')
@section('title', 'Registracija firme')
@section('menusection')
    @include('shared.not_connected')
@endsection
@section('javascript')
    <script language = "JavaScript">
        function goBack() {
            window.location.href = "{{ route('home') }}"
        }
    </script>
@endsection
@section('content')
    <br/><br/>
    <div class="panel panel-info">
        <div class="panel-heading" style="color: #2F3133;" ><h3> &nbsp;&nbsp;&nbsp;Registracija firme</h3></div>
        <br/><br/>

        <div class="panel-body">
            @if(count($errors)>0)
                <ul align="center">
                    @foreach($errors->all() as $error)
                        <li class="alert alert-danger"> {{ $error }} </li>
                    @endforeach
                </ul>

            @endif

            {{--style="color: #2F3133;"   boja za labele--}}

            <form class="form-horizontal" style="align-content: left" action="registerCompany" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <fieldset>
                    <br/>
                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('Username') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label" for="username" style="color: #2F3133;">Naziv kompanije*</label>
                        <div class="col-md-4">
                            <input id="username" name="Username" placeholder="Unesite ime kompanije" class="form-control input-md" value="{{ Request::old('Username') }}" type="text" style="color: #2F3133;">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label" for="password" style="color: #2F3133;">Lozinka*</label>
                        <div class="col-md-4">
                            <input id="password" name="password" placeholder="•••••••••••••" value="{{ Request::old('password') }}" class="form-control input-md" type="password">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('password_confirm') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label" for="passwordagain" style="color: #2F3133;">Lozinka opet*</label>
                        <div class="col-md-4">
                            <input id="passwordagain" name="password_confirm" placeholder="•••••••••••••" value="{{ Request::old('password_confirm') }}" class="form-control input-md" type="password">

                        </div>
                    </div>

                    <!--Text input-->
                    <div class="form-group {{ $errors->has('E_mail') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label" for="email" style="color: #2F3133;">E-mail adresa*</label>
                        <div class="col-md-4">
                            <input id="email" name="E_mail" placeholder="Unesite vasu email adresu" value="{{ Request::old('E_mail') }}" class="form-control input-md" type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('telephone') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label" for="telephone" style="color: #2F3133;"> Kontakt telefon* </label>
                        <div class="col-md-4">
                            <input id="telephone" name="telephone" placeholder="Unesite vas kontakt telefon" value="{{ Request::old('telephone') }}" class="form-control input-md" type="text">

                        </div>
                    </div>


                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="sector" style="color: #2F3133;">Sektor*</label>
                        <div class="col-md-4">
                            <select id="sector" name="sector" class="form-control">
                                <option value="IT"> IT </option>
                                <option value="Telekomunikacije"> Telekomunikacije </option>
                                <option value="Elektronika">  Elektronika </option>
                                <option value="Automatika"> Automatika </option>
                            </select>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('website') ? 'has-error' : ''  }}" >
                        <label class="col-md-4 control-label" for="website" style="color: #2F3133;"> Sajt </label>
                        <div class="col-md-4">
                            <input id="website" name="website" placeholder="Adresa vaseg sajta" class="form-control input-md" value="{{ Request::old('website') }}" type="text">

                        </div>
                    </div>

                    <!-- Textarea -->
                    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label" for="description" style="color: #2F3133;"> Opis </label>
                        <div class="col-md-4">
                            <textarea class="form-control" id="description" value="{{ Request::old('description') }}" name="description" onclick="this.value=''"> O kompaniji </textarea>
                        </div>
                    </div>

                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="buttons" style="color: #2F3133;"></label>
                        <div class="col-md-8">
                            <button type="submit" class="btn btn-primary navbar-btn">Potvrdi</button>
                            <button type="button" onclick="goBack()" class="btn btn-default navbar-btn">Ponisti</button>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
@endsection