@extends('master')

@section('title', 'Brisanje ponuda')

@section('javascript')
    <script language = "JavaScript">
        function selectAllUsers() {
            var allToDeact = document.getElementById('alltodeact');
            var allCheckboxes = document.getElementsByName('todeact[]');
            if (allToDeact.checked) {
                for (var i = 0, n = allCheckboxes.length; i < n; i++) {
                    allCheckboxes[i].checked = true;
                }
            }
            else {
                for (var i = 0, n = allCheckboxes.length; i < n; i++) {
                    allCheckboxes[i].checked = false;
                }
            }
        }

        function Confirm() {
            var x;
            x = confirm("Da li ste sigurni da zelite da obrisete oznacene ponude?");
            if (x) {
                document.getElementById("usersform").submit();
            }
        }
    </script>
@endsection
@section('menusection')
    @include('shared.company_header')
@endsection
@section('content')
    <br/><br/>
    <div class="panel panel-info">
        <div class="panel-heading" style="color: #2F3133;" ><h3> &nbsp;&nbsp;Brisanje ponuda </h3></div>
        <br/><br/>

        <div class="panel-body">
            <form action="{{route('deleteOff')}}" method="post" id="usersform">
                <br/><br/>
                <div class="row">
                    <div class="col-md-12">
                        <br/>
                        <table class="table" style="color: #2F3133;">

                            <thead>
                            <th>Sve ponude</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><input type="checkbox" id="alltodeact" name="alltodeact" value="all" onclick="selectAllUsers()"/></th>
                            </thead>

                            <tbody>

                            @foreach ($offers as $offer)

                                <?php
                                $tip = $offer->Type;
                                $slika ="";
                                if ($tip == "I"){
                                    $tip = "Praksa";
                                    $slika = "images/internlogo.jpg";
                                }
                                else
                                {
                                    $tip = "Posao";
                                    $slika = "images/joblogo.jpg";
                                }

                                $employment = $offer->EmploymentType;
                                if ($employment ="F")
                                    $employment ="Full Time pozicija";
                                else
                                    $employment ="Part Time pozicija";
                                ?>

                                <tr>
                                    <td>{{ $tip}}</td>
                                    <td>{{ $offer->Position }}</td>
                                    <td>{{ $offer->Description }}</td>
                                    <td>{{ $employment }}</td>
                                    <td> <input type="checkbox" name="todeact[]" value="{{ $offer->Id }}"/></td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
                <br/><br/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group" align="center">
                            <button type="button" class="btn btn-primary navbar-btn" onclick="Confirm()">Potvrdi</button>
                            <button type="reset" class="btn btn-default navbar-btn">Ponisti</button>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="_token" value="{{Session::token()}}"/>
            </form>

        </div>
    </div>
    <br/> <br/> <br/> <br/><br/> <br/> <br/> <br/><br/> <br/> <br/> <br/><br/> <br/> <br/> <br/><br/><br/>
@endsection