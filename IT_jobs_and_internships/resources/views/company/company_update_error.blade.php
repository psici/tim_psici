@section('homesection')
    <img style="width:100%;height:24%;background:#80944c" src="{{asset('images/companycover.jpg')}}" alt="sfafsa">
@endsection

@extends('master')
@section('title', 'Azuriranje firme')
@section('menusection')
    @include('shared.company_header')
@endsection
@section('content')

    @if(count($errors)>0)
        <br/><br/>

        <ul align="center">
            @foreach($errors->all() as $error)
                <li class="alert alert-danger"> {{ $error }} </li>
            @endforeach
        </ul>

    @endif

    {{--style="color: #2F3133;"   boja za labele--}}
    <br/>
    <h3 align="center">
            <span style="color: #800000; ">
                <font color ="black"> Informacije o firmi </font>
            </span>
    </h3>
    <br/>

    <form class="form-horizontal" style="align-content: left" action="updateCompany" method="post" id="message" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="OldUsername" value="{{ Request::old('OldUsername') }}" lenght="30"/>
        <input type="hidden" name="OldTelephone" value="{{ Request::old('OldTelephone') }}" length="30"/>
        <input type="hidden" name="OldEmail" value="{{ Request::old('OldEmail') }}";/>

        <table width="100%" height="70%" align="center">

            <tr>
                <td>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label"  for="Username" style="width:200px;color: #2F3133;">Ime kompanije</label>
                        <div class="col-md-4">
                            <input type="text" name="Username" value="{{ Request::old('Username') }}" length="30" class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>
                </td>
                <td>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="broj_zaposlenih" style="width:200px;color: #2F3133;">Broj zaposlenih</label>
                        <div class="col-md-4">
                            <input  name="broj_zaposlenih" value="{{ Request::old('broj_zaposlenih')}}" length="20" placeholder="" class="form-control input-md" type="text">

                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label"  for="lozinka" style="width:200px;color: #2F3133;">Lozinka</label>
                        <div class="col-md-4">
                            <input type="password" name="password" value="{{ Request::old('password') }}" length="30"  class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>
                </td>
                <td>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="godina_osnivanja" style="width:200px;color: #2F3133;">Godina osnivanja</label>
                        <div class="col-md-4">
                            <input  name="godina_osnivanja" value="{{ Request::old('godina_osnivanja') }}" length="20" placeholder="" class="form-control input-md" type="text">

                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('password_confirm') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label"  for="lozinka_opet" style="width:200px;color: #2F3133;">Lozinka opet</label>
                        <div class="col-md-4">
                            <input type="password" name="password_confirm" value="{{ Request::old('password_confirm') }}" length="30" class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>
                </td>
                <td>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="specijalnosti" style="width:200px;color: #2F3133;">Specijalnosti</label>
                        <div class="col-md-4">
                            <input  name="specijalnosti" value="{{ Request::old('specijalnosti') }}" length="20" placeholder="" class="form-control input-md" type="text">

                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('E_mail') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label"  for="E_mail" style="width:200px;color: #2F3133;">Email</label>
                        <div class="col-md-4">
                            <input type="text" name="E_mail" value="{{ Request::old('E_mail') }}" length="30" class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>
                </td>
                <td>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="sediste" style="width:200px;color: #2F3133;">Sediste</label>
                        <div class="col-md-4">
                            <input  name="sediste" value="{{ Request::old('sediste') }}" length="20" placeholder="" class="form-control input-md" type="text">
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('Telephone') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label"  for="telefon" style="width:200px;color: #2F3133;">Kontakt telefon</label>
                        <div class="col-md-4">
                            <input type="text" name="telephone" value="{{ Request::old('telephone') }}" length="30" class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>
                </td>
                <td>
                    <!-- Select input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="sektor" style="width:200px;color: #2F3133;">Sektor</label>
                        <div class="col-md-4">
                            <select id="sektor" name="sektor" class="form-control">
                                <option value="IT"> IT </option>
                                <option value="Telekomunikacije"> Telekomunikacije </option>
                                <option value="Elektronika">  Elektronika </option>
                                <option value="Automatika"> Automatika </option>
                            </select>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('website') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label"  for="sajt" style="width:200px;color: #2F3133;">Web sajt</label>
                        <div class="col-md-4">
                            <input type="text" name="website" value="{{ Request::old('website') }}" length="30" class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>
                </td>
                <td>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="slika" style="width:200px;color: #2F3133;">Slika</label>
                        <div class="col-md-4">
                            <input  name="slika" width="300px" placeholder="" class="form-control input-md" type="file" style="width:300px">
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label"  for="opis" style="width:200px;color: #2F3133;"> Opis </label>
                        <div class="col-md-4">
                            <textarea class="form-control" id="opis" name="opis"> "{{ Request::old('description') }}" </textarea>
                        </div>
                    </div>
                </td>
                <td align="center">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="buttons" style="color: #2F3133;"></label>
                        <div class="col-md-8">
                            <button type="submit" class="btn btn-default navbar-btn">Potvrdi</button>
                            <button type="reset" class="btn btn-default navbar-btn">Ponisti</button>
                        </div>
                    </div>
                </td>
            </tr>

        </table>
    </form>
@endsection