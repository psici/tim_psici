@section('homesection')
    <img style="width:100%;height:24%;background:#80944c" src="{{asset('images/profil_cmp_4.jpg')}}" alt="sfafsa">
@endsection

@extends('master')
@section('title', 'Sve firme')
@section('menusection')
    <?php
    if (session_status() == PHP_SESSION_NONE)
        session_start();
    ?>
    @include('shared.'.$_SESSION['Header'])
@endsection
@section('content')

    <?php
    if (!(is_null($companies))){
    foreach($companies as $company){
    ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                <br/>
                <div class="panel panel-info">
                    <div class="panel-heading" style="background-color:#5e5d5d">
                        <h3 class="panel-title"> <font color="white"> Informacije o firmi </font></h3>
                    </div>
                   <?php $path_to_picture = "images/default_company.png";
                         $new_path = 'uploads/'.$company->Username.'-slika.png';
                         $path;
                        if (file_exists($new_path)){
                             $path =  $new_path;
                        }
                        else
                        {
                            $path = $path_to_picture;
                        } ?>
                   <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="{{asset($path)}}" class="img-circle img-responsive"> </div>
                    <div class="panel-body">
                        <div class="row">
                          <div class=" col-md-9 col-lg-9 ">
                                <table style="color: #2F3133;" class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>Ime :</td>
                                        <td><a href="{{ route('company_profile',['id'=>$company->Username]) }}">  <?php echo $company->Username ?> </a></td>
                                    </tr>
                                    <tr>
                                        <td>Sektor :</td>
                                        <td><?php echo $company->Sector?></td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td>Sediste :</td>
                                        <td><?php echo $company->Headquarter ?></td>
                                    </tr>

                                    </tbody>
                                </table>

                                <a href="{{ route('company_offers',['id'=>$company->Username]) }}" class="btn btn-primary">Pogledaj ponude firme </a>

                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } } ?>
    <div class="text-center">
        {!! $companies->links() !!}
    </div>
    <br/><br/><br/><br/>

@endsection