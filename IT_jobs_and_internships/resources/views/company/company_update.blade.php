@extends('master')
@section('title', 'Registracija korisnika')
@section('menusection')
    @include('shared.company_header')
@endsection
@section('content')

    <br/><br/>

    {{--style="color: #2F3133;"   boja za labele--}}

    <div class="panel panel-info">
        <div class="panel-heading" style="color: #2F3133;" ><h3> &nbsp;&nbsp;&nbsp; Azuriranje profila firme </h3></div>
        <br/><br/>

        <div class="panel-body">

            <form action="updateCompany" method="post" class="form-horizontal" enctype="multipart/form-data">
                <fieldset>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="OldUsername" value=<?php echo "'$Username'" ?> lenght="30"/>
                    <input type="hidden" name="OldTelephone" value=<?php echo $Telephone ?> length="30"/>
                    <input type="hidden" name="OldEmail" value=<?php echo $E_mail ?> length="30"/>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label"  for="Username" style="color: #2F3133;">Ime kompanije</label>
                        <div class="col-md-4">
                            <input type="text" name="Username" value=<?php  echo "'$Username'" ?> length="30" class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="broj_zaposlenih" style="color: #2F3133;">Broj zaposlenih</label>
                        <div class="col-md-4">
                            <input  name="broj_zaposlenih" value=<?php echo $NumOfEmployees ?> length="20" placeholder="" class="form-control input-md" type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label"  for="lozinka" style="color: #2F3133;">Lozinka</label>
                        <div class="col-md-4">
                            <input type="password" name="password" value="<?php  echo $Password ?>"  class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>


                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="godina_osnivanja" style="color: #2F3133;">Godina osnivanja</label>
                        <div class="col-md-4">
                            <input  name="godina_osnivanja" value="<?php echo $FoundationYear ?>"  placeholder="" class="form-control input-md" type="text">

                        </div>
                    </div>


                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('password_confirm') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label"  for="lozinka_opet" style="color: #2F3133;">Lozinka opet</label>
                        <div class="col-md-4">
                            <input type="password" name="password_confirm" value=<?php  echo $Password ?> length="30" class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>


                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="specijalnosti" style="color: #2F3133;">Specijalnosti</label>
                        <div class="col-md-4">
                            <input  name="specijalnosti" value=<?php echo "'$FieldOfWork'" ?> length="20" placeholder="" class="form-control input-md" type="text">

                        </div>
                    </div>


                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('E_mail') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label"  for="E_mail" style="color: #2F3133;">Email</label>
                        <div class="col-md-4">
                            <input type="text" name="E_mail" value=<?php  echo $E_mail ?> length="30" class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>


                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="sediste" style="color: #2F3133;">Sediste</label>
                        <div class="col-md-4">
                            <input  name="sediste" value=<?php echo "'$HeadQuarter'" ?> length="20" placeholder="" class="form-control input-md" type="text">
                        </div>
                    </div>


                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('Telephone') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label"  for="telefon" style="color: #2F3133;">Kontakt telefon</label>
                        <div class="col-md-4">
                            <input type="text" name="telephone" value=<?php  echo stripslashes($Telephone) ?> length="30" class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>


                    <!-- Select input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="sektor" style="color: #2F3133;">Sektor</label>
                        <div class="col-md-4">
                            <select id="sektor" name="sektor" class="form-control">
                                <option value="IT"> IT </option>
                                <option value="Telekomunikacije"> Telekomunikacije </option>
                                <option value="Elektronika">  Elektronika </option>
                                <option value="Automatika"> Automatika </option>
                            </select>
                        </div>
                    </div>


                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('website') ? 'has-error' : ''  }}">
                        <label class="col-md-4 control-label"  for="sajt" style="color: #2F3133;">Web sajt</label>
                        <div class="col-md-4">
                            <input type="text" name="website" value=<?php  echo $Site ?> length="30" class="form-control input-md"  style="color: #2F3133;">
                        </div>
                    </div>


                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="slika" style="color: #2F3133;">Slika</label>
                        <div class="col-md-2">
                            <div class="input-group">
                                <input id="slika" name="slika" class="input-file" type="file">

                            </div>
                        </div>
                    </div>


                    <!-- Text input-->
                    <div class="form-group" style="align-content: center">
                        <label class="col-md-4 control-label"  for="opis" style="color: #2F3133;"> Opis </label>
                        <div class="col-md-4">
                            <textarea class="form-control" id="opis" name="opis"> <?php  echo $Description ?> </textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-4 control-label" for="buttons" style="color: #2F3133;"></label>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary navbar-btn">Potvrdi</button>
                            <button type="reset"  class="btn btn-default navbar-btn">Ponisti</button>
                        </div>
                    </div>


                </fieldset>
            </form>

        </div>
    </div>

@endsection