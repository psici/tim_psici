@section('homesection')
    <img style="width:100%;height:24%;background:#80944c" src="{{asset('images/companycover.jpg')}}" alt="sfafsa">
@endsection

@extends('master')
@section('title', 'Dodavanje oglasa')
@section('menusection')
    <?php
    if (session_status() == PHP_SESSION_NONE)
    session_start();
    ?>
    @include('shared.'.$_SESSION['Header'])
@endsection
@section('content')

    @if(count($errors)>0)
        <br/><br/>

        <ul align="center">
            @foreach($errors->all() as $error)
                <li class="alert alert-danger"> {{ $error }} </li>
            @endforeach
        </ul>

    @endif

    {{--style="color: #2F3133;"   boja za labele--}}
    <h3 align="center">
            <span style="color: #800000; ">
                <font color ="black"> Dodavanje oglasa  </font>
            </span>
    </h3>
    <br/>

    <form class="form-horizontal" style="align-content: left" action="OfferAdded" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="Username" value=<?php echo $Username ?>
        <fieldset>

        <!-- Select Basic -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="tip" style="color: #2F3133;"> Tip ponude </label>
            <div class="col-md-4">
                <select id="sector" name="Type" class="form-control">
                    <option value="Praksa"> Praksa </option>
                    <option value="Posao"> Posao </option>
                </select>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group {{ $errors->has('Position') ? 'has-error' : ''  }}" >
            <label class="col-md-4 control-label" for="Position" style="color: #2F3133;"> Pozicija </label>
            <div class="col-md-4">
                <input id="Position" name="Position" placeholder="" class="form-control input-md" value="{{ Request::old('Position') }}" type="text">
            </div>
        </div>


        <!-- Text input-->
        <div class="form-group {{ $errors->has('Experience') ? 'has-error' : ''  }}" >
            <label class="col-md-4 control-label" for="Experience" style="color: #2F3133;"> Potrebno iskustvo </label>
            <div class="col-md-4">
                <input id="Experience" name="Experience" placeholder="" class="form-control input-md" value="{{ Request::old('Experience') }}" type="text">
            </div>
        </div>

        <!-- Select Basic -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="EmploymentType" style="color: #2F3133;"> Radno vrene </label>
            <div class="col-md-4">
                <select id="EmploymentType" name="EmploymentType" class="form-control">
                    <option value="FullTime"> Full Time </option>
                    <option value="PartTime"> Part Time  </option>
                </select>
            </div>
        </div>


        <!-- Text input-->
        <div class="form-group {{ $errors->has('StartDate') ? 'has-error' : ''  }}" >
            <label class="col-md-4 control-label" for="StartDate" style="color: #2F3133;"> Datum pocetka </label>
            <div class="col-md-4">
                <input id="StartDate" name="StartDate" placeholder="" class="form-control input-md" value="{{ Request::old('StartDate') }}" type="date">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group {{ $errors->has('EndDate') ? 'has-error' : ''  }}" >
            <label class="col-md-4 control-label" for="EndDate" style="color: #2F3133;"> Datum kraja </label>
            <div class="col-md-4">
                <input id="EndDate" name="EndDate" placeholder="" class="form-control input-md" value="{{ Request::old('EndDate') }}" type="date">
            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group {{ $errors->has('Qualifications') ? 'has-error' : ''  }}">
            <label class="col-md-4 control-label" for="Qualifications" style="color: #2F3133;"> Obavezne kvalifikacije </label>
            <div class="col-md-4">
                <textarea class="form-control" name="Qualifications" id="Qualifications" value="{{ Request::old('Qualifications') }}" name="description" onclick="this.value=''"></textarea>
            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group {{ $errors->has('Preffered') ? 'has-error' : ''  }}">
            <label class="col-md-4 control-label" for="Preffered" style="color: #2F3133;"> Pozeljne kvalifikacije </label>
            <div class="col-md-4">
                <textarea class="form-control" id="Preffered" name="Preffered" value="{{ Request::old('Preffered') }}" name="description" onclick="this.value=''"></textarea>
            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group {{ $errors->has('Description') ? 'has-error' : ''  }}">
            <label class="col-md-4 control-label" for="description" style="color: #2F3133;"> Opis </label>
            <div class="col-md-4">
                <textarea class="form-control" id="Description" value="{{ Request::old('Description') }}" name="Description" onclick="this.value=''"></textarea>
            </div>
        </div>

        <!-- TextInput-->
        <div class="form-group {{ $errors->has('ExpireDate') ? 'has-error' : ''  }}">
            <label class="col-md-4 control-label" for="ExpireDate" style="color: #2F3133;"> Datum isteka oglasa </label>
            <div class="col-md-4">
                <input id="ExpireDate" name="ExpireDate" placeholder="" class="form-control input-md" value="{{ Request::old('ExpireDate') }}" type="date">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="buttons" style="color: #2F3133;"></label>
            <div class="col-md-8">
                <button type="submit" class="btn btn-default navbar-btn">Potvrdi</button>
                <button type="reset" class="btn btn-default navbar-btn">Ponisti</button>
            </div>
        </div>

        </fieldset>
    </form>
    <br/>
    <br/>
    <br/>
    <br/>
@endsection