@section('homesection')
    <img style="width:100%;height:24%;background:#80944c" src="{{asset('images/profil_cmp_4.jpg')}}" alt="sfafsa">
@endsection

@section('javascript')
    <script language = "JavaScript">
        function Confirm() {
            var x;
            x = confirm("Da li ste sigurni da zelite da deaktivirate Vas profil?");
            if (x) {
                window.location.href = "{{ route('company.deactivate',['id'=> $Username ]) }}"
            }
        }
    </script>
@endsection

@extends('master')
@section('title', 'Profil firme')
@section('menusection')
    <?php
    if (session_status() == PHP_SESSION_NONE)
    session_start();
    ?>
    @include('shared.'.$_SESSION['Header'])
@endsection
@section('content')

    <?php
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
     if ($_SESSION['Greska']!='')
         echo $_SESSION['Greska'];
     $_SESSION['Greska'] ='';
    ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                <br/>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Informacije o kompaniji </h3>
                    </div>
                    <input type="hidden" id="jsfirma" name="jsfirma" value=<?php echo "$Username" ?> />
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="{{asset($Picture)}}" class="img-circle img-responsive"> </div>

                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information" style="color: #2F3133;" >
                                    <tbody>
                                    <tr>
                                        <td>Ime :</td>
                                        <td> <?php echo "$Username" ?> </td>
                                    </tr>
                                    <tr>
                                        <td>Sektor :</td>
                                        <td><?php echo $Sector ?></td>
                                    </tr>
                                    <tr>
                                        <td>Velicina kompanije :</td>
                                        <td><?php echo $NumOfEmployees ?> zaposlenih </td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td>Sediste :</td>
                                        <td><?php echo $HeadQuarter ?></td>
                                    </tr>
                                    <tr>
                                        <td>Godina osnivanja :</td>
                                        <td> <?php echo $FoundationYear ?> </td>
                                    </tr>
                                    <tr>
                                        <td>Email : </td>
                                        <td><?php echo $E_mail ?></td>
                                    </tr>
                                    <tr>
                                        <td> Opis firme </td>
                                        <td> <?php echo $Description ?> </td>

                                    </tr>
                                    <tr>
                                        <td> Interesovanja : </td>
                                        <td> <?php echo $FieldOfWork ?> </td>
                                    </tr>
                                    <tr>
                                        <td> Telefon </td>
                                        <td> <?php echo "$Telephone" ?></td>
                                    </tr>
                                    </tbody>
                                </table>

                                <a href="{{ route('company_offers',['id'=>$Username]) }}" class="btn btn-primary">Pogledaj ponude firme </a>
                                <a href="<?php echo 'mailto:'.$E_mail ?> " class="btn btn-primary"> Kontaktiraj firmu </a>

                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                            <a href="<?php echo 'mailto:'.$E_mail ?> " data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                        <span class="pull-right">
                            <a href="{{URL::route('createAccount',['Username' => $Username])}}"  data-original-title="Edit this user"  data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                            <a href="javascript:Confirm()" data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                <br/>
                <div class="panel panel-info">
                    <div class="panel-heading" style="background-color:#fce06f";>
                        <h3 class="panel-title"> Oceni kompaniju </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3"> <img alt="User Pic" src="{{asset('images/starrate.png')}}" class="img-circle img-responsive"> </div>
                            <div class=" col-md-9 col-lg-9 ">
                                <form name="formaocena" method="post" action="RateFirm" >
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="Username" value=<?php echo "'$Username'" ?> lenght="30">
                                <table style="color: #2F3133;"  class="table table-user-information" align="center">
                                    <tbody align="center">
                                    <tr align="center">
                                        <td align="center">Prosecna ocena :</td>
                                        <td align="center"> <?php echo $AverageGrade ?> </td>
                                    </tr>
                                    <tr>
                                        <td align="center"> Ukupno glasova :</td>
                                        <td align="center"> <?php echo $GradeNumber ?>  </td>
                                    </tr>
                                    <tr>
                                        <td align="center"> Oceni kompaniju </td>
                                        <td> &nbsp; </td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <input type="radio" id="glas" name="ocena" value="1"/> 1
                                            <input type="radio" id="glas" name="ocena" value="2"/> 2
                                            <input type="radio" id="glas" name="ocena" value="3"/> 3
                                            <input type="radio" id="glas" name="ocena" value="4"/> 4
                                            <input type="radio" id="glas" name="ocena" value="5"/> 5
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <a href="#" class="btn btn-primary" align="center" onclick="document.forms['formaocena'].submit(); return false;" > Glasaj </a>
                                        </td>
                                    </tr>
                                </table>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        </div>
    </div>
    <!-- Textarea -->
    <form name="komentarforma" action="AddComment" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="Username" value=<?php echo "'$Username'" ?> lenght="30"/>
    <table style="width:120%;height:10%;margin-left:30%;">
      <div class="form-group">
          <tr>
              <td>
                <label class="col-md-4 control-label" for="description" style="color: #2F3133;"> Dodaj komentar </label>
              </td>
          </tr>
          <tr>
              <td>
                  <div class="col-md-4">
                      <textarea class="form-control" id="description" name="comment" onclick="this.value='';"> Unesite vas komentar </textarea>
                  </div>
              </td>
          </tr>
          <tr>
              <td>
                      <div class="col-md-8">
                          <button type="submit" class="btn btn-default navbar-btn">Potvrdi</button>
                      </div>
              </td>
          </tr>
      </div>
    </table>
    </form>
    <br/><br/>

    <h4 align="center"> <font color="black"> Komentari korisnika </font></h4>
        <br/>
    <?php
    if (!(is_null($Komentari))){
        foreach($Komentari as $komentar){
        ?>
                <div class="container">
                    <div class="row" style="margin-left:25%;height:20%;">
                        <div class="col-sm-8" align="center" style="height:20%;">
                            <div class="panel panel-white post panel-shadow">
                                <div class="post-heading">
                                    <div class="pull-left image">

                                        <img src="{{asset('images/default_user_comment.jpg')}}" class='img-circle avatar' width="100" height="100" style="margin:10px;" alt='user profile image'>
                                    </div>
                                        <div class='pull-left meta'>
                                            <div class='title h5'>
                                                <a href='#'><b> <?php echo $komentar->UserName ?> </b></a>
                                                               <?php echo "je komentarisao"; ?>
                                            </div>
                                                <h6 class='text-muted time'> <?php echo $komentar->created_at ?></h6>
                                        </div>
                                    </div>
                                    <br/><br/><br/>
                                    <div class='post-description'>
                                        <p style='padding:10px;'> <font color='navy'> <?php echo $komentar->Description ?></font> </p>
                                            <div class='stats'>
                                             <table>
                                                <tr>
                                                    <td>
                                                        <form width="100%"; name = <?php echo "glasza".$komentar->Id ?> method="post" action="ThumbUp" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <input type="hidden" name="commentId"  value= <?php echo $komentar->Id ?> lenght="30"/>
                                                            <input type="hidden" name="Username" value=<?php echo "'$Username'" ?> lenght="30"/>
                                                            <a href='#' class='btn btn-default stat-item'>
                                                                <img src="{{asset('images/thumbup.jpg')}}" onclick="document.forms['<?php echo "glasza".$komentar->Id ?>'].submit(); return false;">
                                                                <?php echo $komentar->thumbs_up ?>
                                                            </a>
                                                        </form>
                                                    </td>
                                                     <td>
                                                         &nbsp
                                                     </td>
                                                    <td>
                                                        <form width="100%"; name = <?php echo "glasprotiv".$komentar->Id ?> method="post" action="ThumbDown" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <input type="hidden" name="commentId"  value= <?php echo $komentar->Id ?> lenght="30"/>
                                                            <input type="hidden" name="Username" value=<?php echo "'$Username'" ?> lenght="30"/>
                                                            <a href='#' class='btn btn-default stat-item'>
                                                                <img src="{{asset('images/thumbdown.jpg')}}" onclick="document.forms['<?php echo "glasprotiv".$komentar->Id ?>'].submit(); return false;">
                                                                <?php echo "".$komentar->thumbs_down ?>
                                                            </a>
                                                        </form>
                                                    </td>
                                                </tr>
                                                </table>
                                    </div>
                                </div>
                            </div>
                       </div>
                    </div>
                <br/><br/><br/>
           </div>
    <?php } } ?>

    <br/><br/><br/><br/><br/>

@endsection