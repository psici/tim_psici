@section('homesection')
    <img style="width:100%;height:24%;background:#80944c" src="{{asset('images/profil_cmp_4.jpg')}}" alt="sfafsa">
@endsection

@extends('master')
@section('title', 'Svi korisnici')
@section('menusection')
    <?php
    if (session_status() == PHP_SESSION_NONE)
        session_start();
    ?>
    @include('shared.'.$_SESSION['Header'])
@endsection
@section('content')

    <?php
    if (!(is_null($users))){
    foreach($users as $userInfo){
    ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                <br/>
                <div class="panel panel-info">
                    <div class="panel-heading" style="background-color:#5e5d5d">
                        <h3 class="panel-title"> <font color="white"> Informacije o korisniku </font></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center">
                                @if (Storage::disk('local')->has($userInfo->Username . '_picture.jpg'))
                                    <img src="{{ route('profile.image', ['imageName' => $userInfo->Username . '_picture.jpg']) }}" alt="" class="img-circle img-responsive">
                                @endif
                                <br/>
                                <table class="table" style="color: #2F3133;">
                                    <tbody>
                                    <tr>
                                        <td>Broj poena:</td>
                                        <td> {{$userInfo->Points}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information" style="color: #2F3133;">
                                    <tbody>
                                    <tr>
                                        <td>Korisnicko ime:</td>
                                        <td>{{ $userInfo->Username }}</td>
                                    </tr>
                                    <tr>
                                        <td>Ime:</td>
                                        <td>{{ $userInfo->FirstName }}</td>
                                    </tr>
                                    <tr>
                                        <td>Prezime:</td>
                                        <td>{{ $userInfo->LastName }}</td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td>E-mail:</td>
                                        <td>{{ $userInfo->Email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pol:</td>
                                        <td>{{ $userInfo->Gender ? $userInfo->Gender : "-" }}</td>
                                    </tr>
                                    <td>Sajt:</td>
                                    <td>{{ $userInfo->Site ? $userInfo->Site : "-"}} </td>
                                    </tr>
                                    <tr>
                                        <td>Fakultet:</td>
                                        <td>{{ $userInfo->Faculty ? $userInfo->Faculty : "-" }}</td>
                                    </tr>
                                    <tr>
                                        <td>Smer:</td>
                                        <td>{{ $userInfo->Department ? $userInfo->Department : "-" }}</td>
                                    </tr>
                                    <tr>
                                        <td>Znanja i vestine:</td>
                                        <td>{{ $userInfo->Skills ? $userInfo->Skills : "-" }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nagrade:</td>
                                        <td>{{ $userInfo->Awards ? $userInfo->Awards : "-"}}</td>
                                    </tr>
                                    <tr>
                                        <td>Datum rodjenja:</td>
                                        <td>{{ $userInfo->Birthdate ? $userInfo->Birthdate : "-"}}</td>
                                    </tr>
                                    <tr>
                                        <td>CV:<div class ="text-danger">  {{ $errors->first('cverror') }}</div></td>
                                        <td><a href="{{ route('profile.cvdownload', ['fileName' => $userInfo->Username . '_cv.pdf']) }}" class="btn btn-primary">Download</a></td>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } } ?>
    <div class="text-center">
        {!! $users->links() !!}
    </div>
    <br/><br/><br/><br/>

@endsection