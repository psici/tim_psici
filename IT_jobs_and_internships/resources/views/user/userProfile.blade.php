@extends('master')
@section('title', 'Moj profil')
@section('javascript')
    <script language = "JavaScript">
        function Confirm() {
            var x;
            x = confirm("Da li ste sigurni da zelite da deaktivirate Vas profil?");
            if (x) {
                window.location.href = "{{ route('profile.deactivate') }}"
            }
        }
    </script>
@endsection
@section('menusection')
    <?php
    if (session_status() == PHP_SESSION_NONE)
        session_start();
        ?>
    @include('shared.'.$_SESSION['Header'])
@endsection
@section('content')
    {{--dodam ako da ne prikazujem ako ne postoji sa if--}}

    <div class="container" >
        <div class="row">
            <br/>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
                <div class="panel panel-info">
                    <div class="panel-heading" style="color: #2F3133;" >
                        <h3>{{ $userInfo->FirstName }}&nbsp;{{ $userInfo->LastName }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center">
                                    <img src="{{ asset($userInfo->PathPicture) }}" alt="" class="img-circle img-responsive">
                                <br/>
                                <table class="table" style="color: #2F3133;">
                                    <tbody>
                                    <tr>
                                        <td>Broj poena:</td>
                                        <td> {{$userInfo->Points}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information" style="color: #2F3133;">
                                    <tbody>
                                    <tr>
                                        <td>Korisnicko ime:</td>
                                        <td>{{ $userInfo->Username }}</td>
                                    </tr>
                                    <tr>
                                        <td>Ime:</td>
                                        <td>{{ $userInfo->FirstName }}</td>
                                    </tr>
                                    <tr>
                                        <td>Prezime:</td>
                                        <td>{{ $userInfo->LastName }}</td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td>E-mail:</td>
                                        <td>{{ $userInfo->Email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pol:</td>
                                        <td>{{ $userInfo->Gender ? $userInfo->Gender : "-" }}</td>
                                    </tr>
                                    <td>Sajt:</td>
                                    <td>{{ $userInfo->Site ? $userInfo->Site : "-"}} </td>
                                    </tr>
                                    <tr>
                                        <td>Fakultet:</td>
                                        <td>{{ $userInfo->Faculty ? $userInfo->Faculty : "-" }}</td>
                                    </tr>
                                    <tr>
                                        <td>Smer:</td>
                                        <td>{{ $userInfo->Department ? $userInfo->Department : "-" }}</td>
                                    </tr>
                                    <tr>
                                        <td>Znanja i vestine:</td>
                                        <td>{{ $userInfo->Skills ? $userInfo->Skills : "-" }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nagrade:</td>
                                        <td>{{ $userInfo->Awards ? $userInfo->Awards : "-"}}</td>
                                    </tr>
                                    <tr>
                                        <td>Datum rodjenja:</td>
                                        <td>{{ $userInfo->Birthdate ? $userInfo->Birthdate : "-"}}</td>
                                    </tr>
                                    <tr>
                                        <td>CV:<div class ="text-danger">  {{ $errors->first('cverror') }}</div></td>
                                        <td><a href="{{ route('profile.cvdownload', ['fileName' => $userInfo->Username . '_cv.pdf']) }}" class="btn btn-primary">Download</a></td>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="<?php echo 'mailto:'.$userInfo->Email ?> " data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                        <span class="pull-right">
                           <a href="{{route('updateUser')}}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                            <a href="javascript:Confirm()" data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
@endsection