@extends('master')
@section('title', 'Registracija korisnika')
@section('menusection')
    @include('shared.not_connected')
@endsection
@section('javascript')
    <script language = "JavaScript">
        function goBack() {
            window.location.href = "{{ route('home') }}"
        }
    </script>
@endsection
@section('content')

    <br/><br/>

    {{--style="color: #2F3133;"   boja za labele--}}

    <div class="panel panel-info">
        <div class="panel-heading" style="color: #2F3133;" ><h3> &nbsp;&nbsp;&nbsp;Registracija korisnika</h3></div>
        <br/><br/>

        <div class="panel-body">
            <form action="{{ route('usersignup')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                <fieldset>
                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                        <label class="col-md-4 control-label" for="username" style="color: #2F3133;">Korisnicko ime*</label>
                        <div class="col-md-4">
                            <input id="username" name="username" placeholder="Unesite korisniko ime" class="form-control input-md" type="text" style="color: #2F3133;" value="{{ Request::old('username') }}">
                            <div class ="text-danger">  {{ $errors->first('username') }}</div>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
                        <label class="col-md-4 control-label" for="firstname" style="color: #2F3133;">Ime*</label>
                        <div class="col-md-4">
                            <input id="firstname" name="firstname" placeholder="Unesite ime" class="form-control input-md" type="text" value="{{ Request::old('firstname')}}">
                            <div class ="text-danger">  {{ $errors->first('firstname') }}</div>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('lastname') ? 'has-error' : '' }}">
                        <label class="col-md-4 control-label" for="lastname" style="color: #2F3133;">Prezime*</label>
                        <div class="col-md-4">
                            <input id="lastname" name="lastname" placeholder="Unesite prezime" class="form-control input-md" type="text" value="{{ Request::old('lastname')}}">
                            <div class ="text-danger">  {{ $errors->first('lastname') }}</div>
                        </div>
                    </div>


                    <!-- Password input-->
                    <div class="form-group {{ $errors->has('newpass') ? 'has-error' : '' }}">
                        <label class="col-md-4 control-label" for="newpass" style="color: #2F3133;">Lozinka*</label>
                        <div class="col-md-4">
                            <input id="newpass" name="newpass" placeholder="•••••••••••••" class="form-control input-md" type="password">
                            <div class ="text-danger">  {{ $errors->first('newpass') }}</div>
                        </div>
                    </div>

                    <!-- Password input-->
                    <div class="form-group {{ $errors->has('newpassagain') ? 'has-error' : '' }}">
                        <label class="col-md-4 control-label" for="newpassagain" style="color: #2F3133;">Lozinka ponovo*</label>
                        <div class="col-md-4">
                            <input id="newpassagain" name="newpassagain" placeholder="•••••••••••••" class="form-control input-md" type="password">
                            <div class ="text-danger">  {{ $errors->first('newpassagain') }}</div>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label class="col-md-4 control-label" for="email" style="color: #2F3133;">E-mail*</label>
                        <div class="col-md-4">
                            <input id="email" name="email" placeholder="example@ex.com" class="form-control input-md" type="text"  value="{{ Request::old('email')}}">
                            <div class ="text-danger">  {{ $errors->first('email') }}</div>
                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                        <label class="col-md-4 control-label" for="gender" style="color: #2F3133;">Pol</label>
                        <div class="col-md-4">
                            <select id="gender" name="gender" class="form-control"  value="{{ Request::old('gender')}}">
                                <option value="M">Muski</option>
                                <option value="Z">Zenski</option>
                            </select>
                            <div class ="text-danger">  {{ $errors->first('gender') }}</div>
                        </div>
                    </div>


                <!-- Text input-->
                <div class="form-group {{ $errors->has('birthdate') ? 'has-error' : '' }}">
                     <label class="col-md-4 control-label" for="birthdate" style="color: #2F3133;">Datum rodjenja</label>
                     <div class="col-md-4">
                         <input id="birthdate" name="birthdate" placeholder="dd.mm.gggg" class="form-control input-md" type="date"  value="{{ Request::old('birthdate')}}">
                         <div class ="text-danger">  {{ $errors->first('birthdate') }}</div>
                     </div>
                </div>

                <!-- Appended Input-->
                    <div class="form-group {{ $errors->has('cv') ? 'has-error' : '' }}">
                        <label class="col-md-4 control-label" for="cv" style="color: #2F3133;">CV</label>
                        <div class="col-md-2">
                            <div class="input-group">
                                <input id="cv" name="cv" class="input-file" type="file">
                                <div class ="text-danger">  {{ $errors->first('cv') }}</div>
                            </div>
                        </div>
                    </div>

                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="buttons" style="color: #2F3133;"></label>
                        <div class="col-md-8">
                            <button type="submit" class="btn btn-primary navbar-btn">Potvrdi</button>
                            <button type="button" onclick="goBack()" class="btn btn-default navbar-btn">Ponisti</button>
                        </div>
                    </div>
                </fieldset>

                <input type="hidden" name="_token" value="{{Session::token()}}"/>
            </form>
        </div>
    </div>

@endsection