@extends('master')
@section('title', 'Azuriranje informacija')
@section('menusection')
    <?php
    if (session_status() == PHP_SESSION_NONE)
        session_start();
    ?>
    @include('shared.'.$_SESSION['Header'])
@endsection
@section('content')
<br/><br/>
{{--style="color: #2F3133;"   boja za labele--}}
    <div class="panel panel-info">
        <div class="panel-heading" style="color: #2F3133;" ><h3> &nbsp;&nbsp;&nbsp;Azuriranje informacija o korisniku</h3></div>
            <br/><br/>

            <div class="panel-body">
                <form action="{{ route('userupdate')}}" method="post" class="form-horizontal" enctype="multipart/form-data">

                <fieldset>
                <!-- Text input-->
                <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="username" style="color: #2F3133;">Korisnicko ime</label>
                    <div class="col-md-4">
                        <input id="username" name="username" placeholder="Unesite korisniko ime" class="form-control input-md" type="text" value="{{ Request::old('username') ? Request::old('username') : $userInfo->Username}}">
                        <div class ="text-danger">  {{ $errors->first('username') }}</div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="firstname" style="color: #2F3133;">Ime</label>
                    <div class="col-md-4">
                        <input id="firstname" name="firstname" placeholder="Unesite ime" class="form-control input-md" type="text" value="{{ Request::old('firstname') ? Request::old('firstname') : $userInfo->FirstName}}">
                        <div class ="text-danger">  {{ $errors->first('firstname') }}</div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group {{ $errors->has('lastname') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="surname" style="color: #2F3133;">Prezime</label>
                    <div class="col-md-4">
                        <input id="surname" name="lastname" placeholder="Unesite prezime" class="form-control input-md" type="text" value="{{ Request::old('lastname') ? Request::old('lastname') : $userInfo->LastName}}">
                        <div class ="text-danger">  {{ $errors->first('lastname') }}</div>
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group {{ $errors->has('oldpass') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="oldpass" style="color: #2F3133;">Stara lozinka</label>
                    <div class="col-md-4">
                        <input id="oldpass" name="oldpass" placeholder="•••••••••••••" class="form-control input-md" type="password">
                        <div class ="text-danger">  {{ $errors->first('oldpass') }}</div>
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group {{ $errors->has('newpass') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="newpass" style="color: #2F3133;">Nova lozinka</label>
                    <div class="col-md-4">
                        <input id="newpass" name="newpass" placeholder="•••••••••••••" class="form-control input-md" type="password">
                        <div class ="text-danger">  {{ $errors->first('newpass') }}</div>
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group {{ $errors->has('newpassagain') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="newpassagain" style="color: #2F3133;">Nova lozinka ponovo</label>
                    <div class="col-md-4">
                        <input id="newpassagain" name="newpassagain" placeholder="•••••••••••••" class="form-control input-md" type="password">
                        <div class ="text-danger">  {{ $errors->first('newpassagain') }}</div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="email" style="color: #2F3133;">E-mail</label>
                    <div class="col-md-4">
                        <input id="email" name="email" placeholder="example@ex.com" class="form-control input-md" type="text" value="{{ Request::old('email') ? Request::old('email') : $userInfo->Email}}">
                        <div class ="text-danger">  {{ $errors->first('email') }}</div>
                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="gender" style="color: #2F3133;">Pol</label>
                    <div class="col-md-4">
                        <select id="gender" name="gender" class="form-control" value="{{ Request::old('gender') }}">
                            <option value="M">Muski</option>
                            <option value="Z">Zenski</option>
                        </select>
                        <div class ="text-danger">  {{ $errors->first('gender') }}</div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group {{ $errors->has('birthdate') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="birthdate" style="color: #2F3133;">Datum rodjenja</label>
                    <div class="col-md-4">
                        <input id="birthdate" name="birthdate" placeholder="dd.mm.gggg" class="form-control input-md" type="date" value="{{ Request::old('birthdate') ? Request::old('birthdate') : $userInfo->Birthdate}}">
                        <div class ="text-danger">  {{ $errors->first('birthdate') }}</div>
                    </div>
                </div>

                <!-- Appended Input-->
                <div class="form-group {{ $errors->has('cv') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="cv" style="color: #2F3133;">CV</label>
                    <div class="col-md-2">
                        <div class="input-group">
                            <input id="cv" name="cv" class="input-file" type="file" value="{{ Request::old('cv') }}">
                            <div class ="text-danger">  {{ $errors->first('cv') }}</div>
                        </div>
                    </div>
                </div>

                <!-- File Button -->
                <div class="form-group {{ $errors->has('picture') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="picture" style="color: #2F3133;">Slika</label>
                    <div class="col-md-2">
                        <div class="input-group">
                            <input id="picture" name="picture" class="input-file" type="file" value="{{ Request::old('picture') }}">
                            <div class ="text-danger">  {{ $errors->first('picture') }}</div>
                        </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group {{ $errors->has('site') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="site" style="color: #2F3133;">Sajt</label>
                    <div class="col-md-4">
                        <input id="site" name="site" placeholder="www.example.com" class="form-control input-md" type="text" value="{{ Request::old('site') ? Request::old('site') : $userInfo->Site }}">
                        <div class ="text-danger">  {{ $errors->first('site') }}</div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group {{ $errors->has('faculty') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="faculty" style="color: #2F3133;">Fakultet</label>
                    <div class="col-md-4">
                        <input id="faculty" name="faculty" placeholder="" class="form-control input-md" type="text" value="{{ Request::old('faculty') ? Request::old('faculty') : $userInfo->Faculty }}">
                        <div class ="text-danger">  {{ $errors->first('faculty') }}</div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group {{ $errors->has('department') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="department" style="color: #2F3133;">Smer</label>
                    <div class="col-md-4">
                        <input id="department" name="department" placeholder="" class="form-control input-md" type="text" value="{{ Request::old('department') ? Request::old('Department') : $userInfo->Department }}">
                        <div class ="text-danger">  {{ $errors->first('department') }}</div>
                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group {{ $errors->has('skills') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="skills" style="color: #2F3133;">Znanja i vestine</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="skills" name="skills">{{ Request::old('skills') ? Request::old('skills') : $userInfo->Skills }}</textarea>
                        <div class ="text-danger">  {{ $errors->first('skills') }}</div>
                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group {{ $errors->has('experience') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="experience" style="color: #2F3133;">Iskustva</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="experience" name="experience">{{ Request::old('experience') ? Request::old('experience') : $userInfo->Experience }}</textarea>
                        <div class ="text-danger">  {{ $errors->first('experience') }}</div>
                    </div>
                </div>

                {{--Da vidim kako cemo tacno deo sa projektima--}}
                {{--<div class="form-group {{ $errors->has('project') ? 'has-error' : '' }}">--}}
                    {{--<label class="col-md-4 control-label" for="project" style="color: #2F3133;">Link ka projektima </label>--}}
                    {{--<div class="col-md-2">--}}
                        {{--<div class="input-group">--}}
                            {{--<input id="project" name="project" class="input-file" type="file" value="{{ Request::old('project') }}">--}}
                            {{--<div class ="text-danger">  {{ $errors->first('project') }}</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}


                <!-- Text input-->
                <div class="form-group {{ $errors->has('awards') ? 'has-error' : '' }}">
                    <label class="col-md-4 control-label" for="awards" style="color: #2F3133;">Nagrade</label>
                    <div class="col-md-4">
                        <textarea id="awards" name="awards" placeholder="" class="form-control input-md"> {{ Request::old('awards') ? Request::old('awards') : $userInfo->Awards }} </textarea>
                        <div class ="text-danger">  {{ $errors->first('awards') }}</div>
                    </div>
                </div>

                    {{--Premestila na profil--}}
                {{--<div class="form-group">--}}
                    {{--<label class="col-md-4 control-label" for="deact" style="color: #2F3133;"></label>--}}
                    {{--<div class="col-md-8">--}}
                        {{--<a href="javascript:Confirm()"> Deaktiviraj profil </a>--}}
                        {{--<a href="{{ route('profile.deactivate') }}"> Deaktiviraj profil </a>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <!-- Button (Double) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="buttons" style="color: #2F3133;"></label>
                    <div class="col-md-8">
                        <button type="submit" class="btn btn-primary navbar-btn">Potvrdi</button>
                        <button type="reset" class="btn btn-default navbar-btn">Ponisti</button>
                    </div>
                </div>
                </fieldset>
                <input type="hidden" name="_token" value="{{Session::token()}}"/>
                </form>
            </div>
        </div>
    </div>

@endsection