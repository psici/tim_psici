@extends('master')

@section('title', 'Rang lista')

@section('menusection')
    <?php
    if (session_status() == PHP_SESSION_NONE)
        session_start();
        ?>
    @include('shared.'.$_SESSION['Header'])
@endsection
@section('content')
    <br/><br/>
    <div class="panel panel-info">
        <div class="panel-heading" style="color: #2F3133;" ><h3> &nbsp;&nbsp;&nbsp;Rang lista korisnika </h3></div>
        <br/><br/>
        <div class="panel-body">
            <form action="" method="" id="rankList">

                <div class="row">
                    <div class="col-md-12">
                        <br/>
                        <table class="table" style="color: #2F3133;">
                            <thead>
                            <th>Rang</th>
                            <th>Ime </th>
                            <th>Prezime </th>
                            <th>Poeni</th>
                            </thead>

                            <tbody>

                            @foreach ($userList as $user)
                                    <tr>
                                        <td>
                                            @if ($user->Rank)
                                                {{ $user->Rank }}
                                            @else 1
                                            @endif
                                        </td>
                                        <td>{{ $user->FirstName }}</td>
                                        <td>{{ $user->LastName }}</td>
                                        <td> {{$user->Points}}</td>
                                    </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $userList->links() !!}
                        </div>
                    </div>
                </div>
                <br/><br/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group" align="center">
                            <?php
                            if (session_status() == PHP_SESSION_NONE) {
                                session_start();
                            }

                            if ($_SESSION['Header']=='company_header')
                                $tip="f";
                            else
                                $tip="k";
                            ?>
                            <?php if ($tip == 'k'){ ?>
                                <p style="color: #2F3133;" >Vas rang je:&nbsp;&nbsp;
                                   {{Auth::user()->Rank}}&nbsp;&nbsp;
                            {{ Auth::user()->FirstName }} &nbsp;
                            {{ Auth::user()->LastName }} &nbsp;
                                {{Auth::user()->Points}}</p></p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
    </br>
@endsection