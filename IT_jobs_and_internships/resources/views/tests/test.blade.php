@extends('master')
@section('title', 'Izrada testa')
@section('javascript')
    <script type='text/javascript'>
        var timerID = 0;
        var counter  = 15;

        function formSubmition()
        {
            document.getElementById("theTest").submit();
        }

        function UpdateTimer() {
            if(counter==0)
                Stop();
            else
            {
                counter--;
                document.theTest.theTime.value=""+counter;
                timerID = setTimeout("UpdateTimer()", 1000);
            }


        }

        function Start() {
            document.theTest.theTime.value="15";
            timerID  = setTimeout("UpdateTimer()", 1000);
        }

        function Stop() {
            if(timerID) {
                clearTimeout(timerID);
                timerID  = 0;
            }
            counter=120;
            alert('Vase vreme je isteklo!');
            formSubmition();
        }
    </script>
@endsection
@section('menusection')
    @include('shared.navbar')
@endsection
@section('content')
    <form class="form-horizontal" name="theTest" id="theTest" method="post" action="{{route('checkAnswers')}}">
        <h2><b>Test</b></h2>
        <b>Preostalo vreme:</b>&nbsp;&nbsp;<input type="text" id="theTime" name="theTime" disabled size="3"/>
        <table class="table table-user-information" align="center">
            <tbody align="center">
                <tr align="center">
                    <td colspan="2" align="center"><p><b><font color="black"> 1.{{explode(PHP_EOL,$q1->QText)[0]}}</font></b></p></td>
                </tr>
                <tr rowspan="3" align="center">
                    <td colspan="2">
                        <table align="center">
                            <tr align="center">
                                <td align="center">
                                    <input type="radio" name="answer" id="answer-0" value="0">
                                </td>
                                <td align="left"><font color="black"> 
                                    {{explode(PHP_EOL,$q1->QText)[1]}}
                                </font></td>
                            </tr>
                            <tr align="center">
                                <td align="center">
                                    <input type="radio" name="answer" id="answer-1" value="1">
                                </td>
                                <td align="left"><font color="black"> 
                                    {{explode(PHP_EOL,$q1->QText)[2]}}
                                </font></td>
                            </tr>
                            <tr align="center">
                                <td align="center">
                                    <input type="radio" name="answer" id="answer-2" value="2">
                                </td>
                                <td align="left"><font color="black"> 
                                    {{explode(PHP_EOL,$q1->QText)[3]}}
                                </font></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="2" align="center"><p><b><font color="black">  2.{{explode(PHP_EOL,$q2->QText)[0]}}</font></b></p></td>
                </tr>
                <tr rowspan="3" align="center">
                    <td colspan="2">
                    <table align="center">
                        <tr align="center">
                            <td align="center">
                                <input type="checkbox" name="answers[]" id="answers-0" value="0">
                            </td>
                            <td align="left"><font color="black"> 
                                {{explode(PHP_EOL,$q2->QText)[1]}}
                            </font></td>
                        </tr>
                        <tr align="center">
                            <td align="center">
                                <input type="checkbox" name="answers[]" id="answers-1" value="1">
                            </td>
                            <td align="left"><font color="black"> 
                                {{explode(PHP_EOL,$q2->QText)[2]}}
                            </font></td>
                        </tr>
                        <tr align="center">
                            <td align="center">
                                <input type="checkbox" name="answers[]" id="answers-2" value="2">
                            </td>
                            <td align="left"><font color="black"> 
                                {{explode(PHP_EOL,$q2->QText)[3]}}
                            </font></td>
                        </tr>
                    </table>
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="2" align="center"><p><b><font color="black">  3.{{explode(PHP_EOL,$q3->QText)[0]}}</font></b></p></td>
                </tr>
                <tr align="center">
                   <td colspan="2" align="center"><font color="black"> 
                       {{explode(PHP_EOL,$q3->QText)[1]}}
                        <input id="txtanswer" name="txtanswer" type="text" placeholder=""  size="50" value="">
                    </font></td>
                </tr>
                <tr align="center">
                    <td colspan="2" align="center">

                            <button type="button" id="confirm" name="confirm" class="btn btn-success" onclick="formSubmition()">Potvrdi</button>
                            <button type="reset" id="cancel" name="cancel" class="btn btn-warning">Ponisti</button>

                    </td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="_token" value="{{Session::token()}}"/>
    </form>
    <script type='text/javascript'>
        Start();
    </script>
    @if($errors->any())
       <?php echo $errors->first(); ?>
        <script type='text/javascript'>
            window.location = "{{ url('/profil_korisnika/') }}";
        </script>
    @endif
@endsection