@extends('master')
@section('title', 'Dodavanje pitalica')
@section('javascript')
    <script type='text/javascript'>
        function showAlert() {
            alert('Pitalica je uspesno dodata');
        };
    </script>
@endsection
@section('menusection')
    @include('shared.admin_header')
@endsection
@section('content')

    <form class="form-horizontal" name="addQuestion" id="addQuestion" action="{{ route('add')}}" method="post">
        <fieldset>

            </br>
            </br>
            </br>
            </br>


            <!-- Textarea -->
            <div class="form-group">
                <label style="color: #2F3133;"  class="col-md-4 control-label" for="question">Pitanje</label>
                <div class="col-md-4">
                    <textarea class="form-control" id="question" name="question" placeholder="Unesite tekst pitanja" required></textarea>
                </div>
            </div>
            </br>
            <!-- Multiple Radios -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="answerType"></label>
                <div class="col-md-4">
                    <div class="radio">
                        <label style="color: #2F3133;"  for="answerType-0">
                            <input type="radio" name="answerType" id="answerType-0" value="1" required >
                            Jedan tacan odgovor
                        </label>
                    </div>
                    <div class="radio">
                        <label style="color: #2F3133;"  for="answerType-1">
                            <input type="radio" name="answerType" id="answerType-1" value="2" required>
                            Vise tacnih odgovora
                        </label>
                    </div>
                    <div class="radio">
                        <label style="color: #2F3133;"  for="answerType-2">
                            <input type="radio" name="answerType" id="answerType-2" value="3" required>
                            Unos odgovora tekstom
                        </label>
                    </div>
                </div>
            </div>
            </br>
            <!-- Textarea -->
            <div class="form-group">
                <label style="color: #2F3133;" class="col-md-4 control-label" for="answers">Odgovor/i</label>
                <div class="col-md-4">
                    <textarea class="form-control" id="answers" name="answers" placeholder="Unesite odgovor/e" required></textarea>
                </div>
            </div>
            </br> </br>
            <!-- Button (Double) -->
            <div class="form-group">
                <label style="color: #2F3133;" class="col-md-4 control-label" for="confirm"></label>
                <div class="col-md-8">
                    <button type="submit" id="confirm" name="confirm" class="btn btn-success" >Potvrdi</button>
                    <button type="reset" id="cancel" name="cancel" class="btn btn-warning">Ponisti</button>
                </div>
            </div>

            <div id="success" name="success">
                @if($errors->any())
                    <script type='text/javascript'>
                       showAlert();
                    </script>
                @endif
            </div>

        </fieldset>

        </br>
        </br>
        </br>
        <input type="hidden" name="_token" value="{{Session::token()}}"/>
    </form>


@endsection
