@extends('master')

@section('title', 'Brisanje komentara')

@section('javascript')
    <script language = "JavaScript">
        function Confirm() {
            var x;
            x = confirm("Da li ste sigurni da zelite da obrisete oznacene komentare?");
            if (x) {
                document.getElementById("usersform").submit();
            }
        }
    </script>
@endsection

@section('menusection')
    @include('shared.admin_header')
@endsection

@section('content')
    <h4 align="center"> <font color="black"> Komentari korisnika </font></h4>
    <br/>

    <form action="{{route('deleteCom')}}" method="get" id="usersform">
        <table>
            <tbody>
            @foreach ($comments as $komentar)
            <tr>
            <div class="container">
                <div class="row" style="margin-left:25%;height:20%;">
                    <div class="col-sm-8" align="center" style="height:20%;">
                        <div class="panel panel-white post panel-shadow">
                            <div class="post-heading">
                                <div class="pull-left image">
                                    <img src="{{asset('images/default_user_comment.jpg')}}" class='img-circle avatar' width="100" height="100" style="margin:10px;" alt='user profile image'>
                                </div>
                                <div class='pull-left meta'>
                                    <div class='title h5'>
                                        <a href='#'><b> <?php echo $komentar->UserName ?> </b></a>
                                        <?php echo "je komentarisao"; ?>
                                    </div>
                                    <h6 class='text-muted time'> <?php echo $komentar->created_at ?></h6>

                                </div>
                            </div>
                            <br/><br/><br/>
                            <div class='post-description'>
                                <p style='padding:10px;'> <font color='navy'> <?php echo $komentar->Description ?></font> </p>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" name="todeact[]" value="{{ $komentar->Id }}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
            </div>
            </tr>
            @endforeach
            <tbody>
        </table>
    </form>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group" align="center">
                <button type="submit" class="btn btn-primary navbar-btn" value = "delete" onclick="Confirm()">Potvrdi</button>
                <button type="reset" class="btn btn-default navbar-btn">Ponisti</button>
            </div>
        </div>
    </div>
    <div class="text-center">
        {!! $comments->links() !!}
    </div>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/> <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

@endsection