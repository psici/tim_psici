
    <form action="{{route('changeUserToAdmin')}}" method="post" id="usersform">
        <br/><br/>
        <div class="row">
            <div class="col-md-12">
                <br/>
                <table class="table" style="color: #2F3133;">
                    <thead>
                    <th>Korisnicko ime</th>
                    <th>Ime </th>
                    <th>Prezime </th>
                    <th><input type="checkbox" id="alltoadmin" name="alltoadmin" value="all" onclick="selectAllUsers()"/></th>
                    </thead>

                    <tbody>

                    @foreach ($userList as $user)
                        @if ($user->IsAdmin == 0)
                            <tr>
                                <td>{{ $user->Username }}</td>
                                <td>{{ $user->FirstName }}</td>
                                <td>{{ $user->LastName }}</td>
                                <td> <input type="checkbox" name="toadmin[]" value="{{ $user->Username }}"/></td>
                            </tr>
                        @endif
                    @endforeach

                    </tbody>
                </table>
                <div class="text-center">
                    {!! $userList->links() !!}
                </div>
            </div>
        </div>
        <br/><br/>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group" align="center">
                    <button type="button" onclick="Confirm()" class="btn btn-primary navbar-btn" >Potvrdi</button>
                    <button type="reset" class="btn btn-default navbar-btn">Ponisti</button>
                </div>
            </div>
        </div>

        <input type="hidden" name="_token" value="{{Session::token()}}"/>
    </form>

