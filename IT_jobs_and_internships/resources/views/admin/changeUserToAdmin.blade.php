@extends('master')

@section('title', 'Promena statusa naloga')

@section('javascript')
    <script language = "JavaScript">
       function selectAllUsers() {
           var allToAdmin = document.getElementById('alltoadmin');
           var allCheckboxes = document.getElementsByName('toadmin[]');
           if (allToAdmin.checked) {
               for (var i = 0, n = allCheckboxes.length; i < n; i++) {
                   allCheckboxes[i].checked = true;
               }
           }
           else {
               for (var i = 0, n = allCheckboxes.length; i < n; i++) {
                   allCheckboxes[i].checked = false;
               }
           }
       }
       function Confirm() {
           var x;
           x = confirm("Da li ste sigurni da da promenite status oznacenih naloga u administratorske?");
           if (x) {
               document.getElementById("usersform").submit();
           }
       }

       $(function() {
           $("#theForm").submit(function(e) {
//               alert('submit');
               e.preventDefault();

               $.ajaxSetup({
                   headers:{
                       'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                   }
               });

              /* var $inputs = $('#theForm :input');

                // not sure if you wanted this, but I thought I'd add it.
                // get an associative array of just the values.
                var values = {};
                $inputs.each(function() {
                values[this.name] = $(this).val();
                });
                var str=values['theSearch'];*/
               var str=$('#theForm').serialize();
//               alert(str);
               var url=$(this).attr('action');
               //var url='/nadjiKorisnike/'+str;
//               alert(url);
               var get=$(this).attr('method');
//               alert(get);
               $.ajax({
                   type:get,
                   url: url,
                   data:str,
                   //dataType:'jsonp',
                   //processData: false,
                   //contentType: "application/json; charset=UTF-8",
                   //complete: callback,
                   //async : false,
                   success:function(result){
//                       alert('success');
//                       if(result==null)
//                           alert('null');
//                       else
//                           console.log(result);
                       document.getElementById("searchResult").innerHTML=result;
                   },
                   error:function(qXHR, textStatus, errorThrown){
                       alert(errorThrown);
                       alert(textStatus);
                   }
               });

           });});


        $(function() {
        $(document).on('click','.pagination a',function(e){
//            alert('test');
            e.preventDefault();
            var page=$(this).attr('href').split('page=')[1];
            getUser(page,$('#theSearch').val());
        })
        });

        function getUser(page,search)
        {
            var url="{{url('/nadjiKorisnikePaginacija')}}";
            $.ajax({
                type:'get',
                url: url+'?page='+page,
                data:{'theSearch':search}
            }).done(function(data){
                document.getElementById("searchResult").innerHTML=data;
            })
        }

    </script>
@endsection
@section('menusection')
    @include('shared.admin_header')
@endsection
@section('content')
    <br/><br/>
    <div class="panel panel-info">
        <div class="panel-heading" style="color: #2F3133;" ><h3> &nbsp;&nbsp;&nbsp;Promena statusa naloga u administratorski </h3></div>
        <br/><br/>
        <form class="navbar-form navbar-left" id="theForm" name="theForm" action="{{url('/nadjiKorisnike')}}" method="get">
            <div class="form-group">
                <input type="text" name="theSearch" id="theSearch" placeholder="Unesite korisnicko ime" class="form-control">
            </div>
            <button type="submit" class="btn btn-default">Pretraga</button>
            <br/>
            <input type="hidden" name="_token" value="{{Session::token()}}"/>
        </form>
        <div class="panel-body" id="searchResult" name="searchResult">
        @include('admin.content',['userList' => $userList])
        </div>
</div>
<br/>
@endsection