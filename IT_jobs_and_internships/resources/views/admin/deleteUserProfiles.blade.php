@extends('master')

@section('title', 'Brisanje naloga')

@section('javascript')
    <script language = "JavaScript">
        function selectAllUsers() {
            var allToDeact = document.getElementById('alltodeact');
            var allCheckboxes = document.getElementsByName('todeact[]');
            if (allToDeact.checked) {
                for (var i = 0, n = allCheckboxes.length; i < n; i++) {
                    allCheckboxes[i].checked = true;
                }
            }
            else {
                for (var i = 0, n = allCheckboxes.length; i < n; i++) {
                    allCheckboxes[i].checked = false;
                }
            }
        }

        function Confirm() {
            var x;
            x = confirm("Da li ste sigurni da zelite da deaktivirate oznacene naloge?");
            if (x) {
                document.getElementById("usersform").submit();
            }
        }
    </script>
@endsection
@section('menusection')
    @include('shared.admin_header')
@endsection
@section('content')
    <br/><br/>
    <div class="panel panel-info">
        <div class="panel-heading" style="color: #2F3133;" ><h3> &nbsp;&nbsp;Deaktiviranje naloga </h3></div>
        <br/><br/>
        <div class="panel-body">
            <form action="{{route('deactivateUserProfiles')}}" method="post" id="usersform">
                <br/><br/>
                <div class="row">
                    <div class="col-md-12">
                        <br/>
                        <table class="table" style="color: #2F3133;">
                            <thead>
                            @if ($type == 'user')
                                <th>Korisnicko ime</th>
                                <th>Ime </th>
                                <th>Prezime </th>
                                <th><input type="checkbox" id="alltodeact" name="alltodeact" value="all" onclick="selectAllUsers()"/></th>
                                </thead>
                            @else
                                <th>Naziv kompanije</th>
                                <th><input type="checkbox" id="alltodeact" name="alltodeact" value="all" onclick="selectAllUsers()"/></th>
                                </thead>
                            @endif
                            <tbody>

                            @foreach ($userList as $user)
                                <tr>
                                    @if ($type == 'user')
                                        <td>{{ $user->Username }}</td>
                                        <td>{{ $user->FirstName }}</td>
                                        <td>{{ $user->LastName }}</td>
                                        <td> <input type="checkbox" name="todeact[]" value="{{ $user->Username }}"/></td>
                                    @else
                                        <td>{{ $user->Username }}</td>
                                        <td> <input type="checkbox" name="todeact[]" value="{{ $user->Username }}"/></td>
                                    @endif
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $userList->links() !!}
                        </div>
                    </div>
                </div>
                <br/><br/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group" align="center">
                            <button type="button" class="btn btn-primary navbar-btn" onclick="Confirm()">Potvrdi</button>
                            <button type="reset" class="btn btn-default navbar-btn">Ponisti</button>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="_token" value="{{Session::token()}}"/>
            </form>

        </div>
    </div>
    <br/>
@endsection