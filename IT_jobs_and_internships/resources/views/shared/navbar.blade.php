<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Logo and responsive toggle -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <a class="navbar-brand" href="#">
            <span class="glyphicon glyphicon-fire"></span>
           IT poslovi i prakse&nbsp;&nbsp;&nbsp;&nbsp;
        </a>
        <!-- Navbar links -->
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{route('userprofile')}}">Moj profil</a>
                </li>
                <li>
                    <a href="{{route('jobs')}}">Poslovi</a>
                </li>
                <li>
                    <a href="{{route('internships')}}">Prakse</a>
                </li>
                <li>
                    <a href="{{route('allCompanies')}}">Firme</a>
                </li>
                <li>
                    <a href="{{route('allUsers')}}"> Korisnici </a>
                </li>
                 <li>
                    <a href="{{ route('showTest') }}">Test</a>
                </li>
                <li>
                    <a href="{{ route('rankList') }}">Rang lista</a>
                </li>
                <li>
                    <a href="{{ route('logoutUser') }}">Odjava</a>
                </li>
<!--                Primer za drop down meni-->
<!--                <li class="dropdown">-->
<!--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>-->
<!--                    <ul class="dropdown-menu" aria-labelledby="about-us">-->
<!--                        <li><a href="#">Engage</a></li>-->
<!--                        <li><a href="#">Pontificate</a></li>-->
<!--                        <li><a href="#">Synergize</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>