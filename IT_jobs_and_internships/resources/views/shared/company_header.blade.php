<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Logo and responsive toggle -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>

        <?php
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $profil = $_SESSION['Username']
        ?>

        <a class="navbar-brand" href="#">
            <span class="glyphicon glyphicon-fire"></span>
            IT poslovi i prakse&nbsp;&nbsp;&nbsp;&nbsp;
        </a>
        <!-- Navbar links -->
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{ route('company_profile',['id'=>$profil]) }}"> Moj profil </a>
                </li>
                <li>
                    <a href="{{ route('jobs') }}">Poslovi</a>
                </li>
                <li>
                    <a href="{{ route('internships') }}">Prakse</a>
                </li>
                <li>
                    <a href="{{ route('allCompanies') }}">Firme</a>
                </li>
                <li>
                    <a href="{{route('allUsers')}}"> Korisnici </a>
                </li>
                <li>
                    <a href="{{route('rankList')}}">Rang lista</a>
                </li>
                <li>
                    <a href="{{URL::route('AddOffer',['Username'=>$profil])}}"> Dodavanje oglasa </a>
                </li>
                <li>
                <li>
                    <a href="{{route('deleteOffers')}}">Izbrisi ponude</a>
                </li>
                </li>
                <li>
                    <a href="{{ route('logoutCompany') }}">Odjava</a>
                </li>
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>