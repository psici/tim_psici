<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/27/2017
 * Time: 12:24 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $timestamps = true;
    protected $table = "comment";
}