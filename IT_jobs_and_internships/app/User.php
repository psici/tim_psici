<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;
    //podrazumevana tabela je users sto je u skladu sa bazom nasom
    public $primaryKey = 'Username'; //jer pk nije id
    public $incrementing = false; //je pk nije autoinkremet
    public $timestamps = false; //jer nemamo created_at i updated_at
    protected $fillable = ['Username', 'FirstName', 'LastName', 'Password', 'Email', 'Gender', 'Site', 'Faculty', 'Department',
        'Experience', 'Skills', 'Awards', 'Points', 'Birthdate', 'IsAdmin','Rank', 'PathCV', 'PathPicture'];

}
