<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class company_middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (isset($_SESSION['Username'])) {
            $username = $_SESSION['Username'];
            $companies = DB::table('company')->where('Username', $username)->get();
            if (count($companies)) {
                return $next($request);
            } else {
                return redirect()->back();
            }
        } else {
            return redirect()->back();
        }
    }
}
