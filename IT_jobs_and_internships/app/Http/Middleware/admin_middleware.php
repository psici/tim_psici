<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class admin_middleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::user();
        if ($user != null && $user->IsAdmin == 1) {
            return $next($request);
        } else {
            return redirect()->back();
        }
    }
}
