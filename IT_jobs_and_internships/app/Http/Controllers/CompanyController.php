<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use  App\Company;
use  App\Comment;
use Illuminate\Support\Facades\File;
use  Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;



class CompanyController extends Controller
{

    public function registration()
    {
        return view('company.company_registration');
    }

    public function validateUpdate()
    {
        return view('company.company_update_error');
    }

    public function create(Request $request)
    {
        $this->validate($request,['E_mail'=>'required|email|unique:company',
                                  'Username'=>'required|unique:company|min:1|max:32',
                                  'password'=>'required|min:5|max:30',
                                  'password_confirm' => 'required|same:password',
                                  'sector' => 'required',
                                  'website'=> 'url',
                                  'telephone'=> 'numeric|digits_between:1,20']);

        $company = new Company;

        $company->Username = Input::get("Username");
        $company->Password = Crypt::encryptString(Input::get("password"));
        $company->E_mail = Input::Get("E_mail");
        $company->Sector = Input::Get("sector");
        $company->Description = Input::Get("description");
        $company->Site = Input::Get("website");

        $company->NumOfEmployees = 0;
        $company->FoundationYear = 2017;
        $company->FieldOfWork = "No informations yet";
        $company->HeadQuarter = "No informations yet";


        $company->save();

        $company->Telephone = Input::Get("telephone");


        DB::table('telephone_company')->insert((array('Telephone' => $company->Telephone, 'Username' => $company->Username)));


        $data['Username'] = $company->Username;
        $data['Password'] = Crypt::decryptString($company->Password);
        $data['E_mail'] = $company->E_mail;
        $data['Sector'] = $company->Sector;
        $data['Description'] = $company->Description;
        $data['Telephone'] = $company->Telephone;
        $data['Site'] = $company->Site;
        $data['NumOfEmployees'] = $company->NumOfEmployees;
        $data['FoundationYear'] = $company->FoundationYear;
        $data['FieldOfWork'] = $company->FieldOfWork;
        $data['HeadQuarter'] = $company->HeadQuarter;


        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['Username'] = $company->Username;
        $_SESSION['Greska']='';
        $_SESSION['Header']='company_header';

        return view('company.company_update', $data);
    }

    public function edit(Request $request)
    {
        $path_to_picture = "images/default_company.png";

        $proveraUnique = "";
        $proveraUniqueMail = "";

        if (Input::get("Username") == Input::get("OldUsername"))
            $proveraUnique = "required|min:1|max:30";
        else
        {
            $proveraUnique = "required|min:1|max:30|unique:company";
        }

        if (Input::get("OldEmail") == Input::get("E_mail"))
            $proveraUniqueMail ="required|email";
        else
            $proveraUniqueMail = "required|email|unique:company";


        $this->validate($request,['E_mail'=>''.$proveraUniqueMail,
             'Username'=>''.$proveraUnique,
             'password'=>'required|min:5|max:30',
             'password_confirm' => 'required|same:password',
             'website'=> 'required|url',
             'telephone'=> 'required|numeric|digits_between:1,20']);

        if (!(Input::get("Username") == Input::get("OldUsername"))){
            if (Storage::disk('image')->exists(Input::get("OldUsername").'-slika.png')) {
                $file = Storage::get(Input::get("OldUsername") . '-slika.png');
                $file->move('uploads', Input::get("Username") . '-slika.png');
            }

            DB::table('comment')
                ->where('Company', Input::get("OldUsername"))
                ->update(array('Company' => Input::get("Username")));

        }


        if (Input::hasFile('slika')) {
            Input::file('slika')->move('uploads', Input::get("Username") . '-slika.png');
            $path_to_picture = "uploads/" . Input::get("Username") . '-slika.png';
        }

        DB::table('telephone_company')
            ->where('Username', Input::get("OldUsername"))
            ->where('Telephone', Input::Get("OldTelephone"))
            ->limit(1)
            ->update(array('Username' => Input::get("Username"),
                'Telephone' => Input::get("telephone")));

        DB::table('company')
            ->where('Username', Input::Get("OldUsername"))
            ->limit(1)
            ->update(array('Username' => Input::Get("Username"),
                'Password' => Crypt::encryptString(Input::get("password")),
                'NumOfEmployees' => Input::get("broj_zaposlenih"),
                'FoundationYear' => Input::get("godina_osnivanja"),
                'FieldOfWork' => Input::get("specijalnosti"),
                'E_mail' => Input::get("E_mail"),
                'HeadQuarter' => Input::get("sediste"),
                'Sector' => Input::get("sektor"),
                'Site' => Input::get("website"),
                'Description' => Input::get("opis")));


        $data['Username'] = Input::Get("Username");
        $data['Password'] = Input::Get("password");
        $data['E_mail'] = Input::Get("E_mail");
        $data['Sector'] = Input::Get("sektor");
        $data['Description'] = Input::Get("opis");
        $data['Telephone'] = Input::Get("telephone");
        $data['Site'] = Input::get("website");
        $data['NumOfEmployees'] = Input::get("broj_zaposlenih");
        $data['FoundationYear'] = Input::get("godina_osnivanja");
        $data['FieldOfWork'] = Input::get("specijalnosti");
        $data['HeadQuarter'] = Input::get("sediste");
        $data['Picture'] = $path_to_picture;

        $company = DB::table('company')
            ->where('Username',Input::get("Username"))
            ->first();



        $data['GradeNumber']= $company->GradeNumber;
        $data['AverageGrade'] = $company->AverageGrade;

        $komentari = DB::table('comment')->where('Company', Input::Get("Username"))->orderBy('created_at','desc')->get();

        $data['Komentari'] = $komentari;

        return view('company.company_profile',$data);

    }

    public function comment(){

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if ($_SESSION['Username'] == Input::get("Username")){
            $_SESSION['Greska'] = "<script type='text/javascript'>alert('Nije dozvoljeno dodavati komentare sebi.');</script>";
            return redirect('company_profile/'.Input::get("Username"));
        }


        $comment = new Comment;

        $comment->UserName = $_SESSION['Username'];
        $comment->Company  = Input::get("Username");
        $comment->Description = Input::get("comment");
        $comment->thumbs_up = 0;
        $comment->thumbs_down = 0;

        $comment->save();

        $firma = DB::table('company')->where('Username',Input::get("Username"))->first();

        return redirect('company_profile/'.$firma->Username.'');
    }

    public function profile($id){

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $firma = DB::table('company')->where('Username',$id.'')->first();
        $komentari = DB::table('comment')->where('Company',$id.'')->orderBy('created_at','desc')->get();

        $data['Username'] = $firma->Username;
        $data['Password'] = Crypt::decryptString($firma->Password);
        $data['E_mail']   = $firma->E_mail;
        $data['Sector']   = $firma->Sector;
        $data['Description'] = $firma->Description;
        $telefonRed = $telefon = DB::table('telephone_company')->where("Username",$firma->Username)->first();
        $data['Telephone'] = $telefonRed->Telephone;
        $data['Site']     =  $firma->Site;
        $data['NumOfEmployees'] = $firma->NumOfEmployees;
        $data['FoundationYear'] = $firma->FoundationYear;
        $data['FieldOfWork'] = $firma->FieldOfWork;
        $data['HeadQuarter'] = $firma->Headquarter;
        $data['AverageGrade'] = $firma->AverageGrade;
        $data['GradeNumber'] = $firma->GradeNumber;

        $path_to_picture = "images/default_company.png";

        $new_path = 'uploads/'.$firma->Username.'-slika.png';

        if (file_exists($new_path)){
            $data['Picture'] = $new_path;
        }
        else
        {
            $data['Picture'] = $path_to_picture;
        }

        $data['Komentari'] = $komentari;

        return view('company.company_profile',$data);
    }

    public function ThumbUp(){
        DB::table('comment')
            ->where('Id', Input::get("commentId"))
            ->update(['thumbs_up'=>DB::raw('thumbs_up + 1')]);

        $firma = Input::get("Username");

        return redirect('company_profile/'.$firma);
    }

    public function ThumbDown(){
        DB::table('comment')
            ->where('Id', Input::get("commentId"))
            ->update(['thumbs_down'=>DB::raw('thumbs_down + 1')]);

        $firma = Input::get("Username");

        return redirect('company_profile/'.$firma);
    }

    public function RateFirm(){
        if (empty(Input::get("ocena")))
            $ocena = 5;
        else
            $ocena = Input::get("ocena");

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if ($_SESSION['Username'] == Input::get("Username")){
            $_SESSION['Greska'] = "<script type='text/javascript'>alert('Nije dozvoljeno ocenjivati samog sebe');</script>";
            return redirect('company_profile/'.Input::get("Username"));
        }
        else {
            DB::table('company')
                ->where('Username', Input::get("Username"))
                ->increment('GradeSum', $ocena);

            DB::table('company')
                ->where('Username', Input::get("Username"))
                ->increment('GradeNumber', 1);

            DB::table('company')
                ->where('Username', Input::get("Username"))
                ->update(['AverageGrade' => DB::raw('GradeSum / GradeNumber')]);

            return redirect('company_profile/' . Input::get("Username"));
        }
    }

    public function EditInfo(Request $request){

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if ($_SESSION['Username'] != $request->Username){
            $_SESSION['Greska'] = "<script type='text/javascript'>alert('Nije dozvoljeno azuriranje profila druge kompanije.');</script>";
            return redirect('company_profile/'.Input::get("Username"));
        }

        $company = DB::table('company')
            ->where('Username',$request->Username)
            ->first();

        $data['Username'] = $company->Username;
        $data['Password'] = Crypt::decryptString($company->Password);
        $data['E_mail'] = $company->E_mail;
        $data['Sector'] = $company->Sector;
        $data['Description'] = $company->Description;

            $telefon = DB::table('telephone_company')->where("Username",$request->Username)->first();

        $data['Telephone'] = $telefon->Telephone;
        $data['Site'] = $company->Site;
        $data['NumOfEmployees'] = $company->NumOfEmployees;
        $data['FoundationYear'] = $company->FoundationYear;
        $data['FieldOfWork'] = $company->FieldOfWork;
        $data['HeadQuarter'] = $company->Headquarter;

        return view('company.company_update', $data);
    }


    public function AddOffer(Request $request){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if ($_SESSION['Username'] != $request->Username){
            $_SESSION['Greska'] = "<script type='text/javascript'>alert('Nije dozvoljeno dodavanje ponuda u ime druge firme.');</script>";
            return redirect('company_profile/'.Input::get("Username"));
        }
        $data['Username'] = $request->Username;
        return view('company.company_offer',$data);
    }

    public function OfferAdded(Request $request){

        $this->validate($request,['Experience' => 'required|numeric|digits_between:1,2',
                                  'Position' => 'required',
                                  'StartDate' => 'required',
                                  'ExpireDate' => 'required',
                                  'Qualifications' => 'required'
        ]);

        $EndDate = "";
        if(empty(Input::get("EndDate")))
            $EndDate = "Permanent";
        else
            $EndDate = Input::get("EndDate");

        $preffered = "";;
        if(empty(Input::get("Preffered")))
            $preffered = "No informations yet.";
        else
            $preffered = Input::get("Preffered");

        $tip = Input::get("Type");
        $tipFinal = "";
        if ($tip == "Praksa")
            $tipFinal = 'I';
        else
            $tipFinal = 'J';

        $employmentType = "";
        if (Input::get("EmploymentType")=="FullTime")
            $employmentType = "F";
        else
            $employmentType = "P";

        $description = "";
        if (empty(Input::get("Description")))
            $description = "No informations yet";
        else
            $description = Input::get("Description");

        DB::table('offer')->insert(array(
            'Username' => Input::get("Username"),
            'Type' => $tipFinal,
            'Position' => Input::get("Position"),
            'Experience'=>Input::get("Experience"),
            'EmploymentType' => $employmentType,
            'StartDate' => Input::get("StartDate"),
            'EndDate' => $EndDate,
            'ExpireDate' => Input::get("ExpireDate"),
            'Qualifications' => Input::get("Qualifications"),
            'Preffered' => $preffered,
            'Description' => $description
        ));

        return redirect('company_profile/'.Input::get("Username").'');
    }

    public function SignIn(Request $request){

        $company = DB::table('company')
            ->where('Username',$request->uname)
            ->first();

        if (is_null($company)) {
            return 0;
        }
        else {
            if ($request->password == Crypt::decryptString($company->Password)) {
                if (session_status() == PHP_SESSION_NONE) {
                    session_start();
                }
                $_SESSION['Username'] = $request->uname;
                $_SESSION['Greska'] = '';
                $_SESSION['Header'] = 'company_header';
                return 1;
            } else
                return 0;
        }
    }

    public function deactivate(Request $request){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if ($_SESSION['Username'] != $request->id){
            $_SESSION['Greska'] = "<script type='text/javascript'>alert('Nije dozvoljeno deaktivirati tudj profil.');</script>";
            return redirect('company_profile/'.$request->id);
        }
        DB::table('company')
            ->where('Username',$request->id)
            ->delete();
        session_destroy();
        return redirect("");
    }

    public function logoutCompany()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['Username']="";
        session_destroy();
        return redirect()->route('home');
    }

    public function offers(Request $request)
    {
        $offers = DB::table('offer')->where('Username',$request->id)->get();
        $firma = DB::table('company')->where('Username',$request->id)->first();
        $email = $firma->E_mail;

        $data['Offers'] = $offers;
        $data['Email'] = $email;

        return view('company.company_offers_list',$data);

    }

    public function jobs(Request $request)
    {
        $offers = DB::table('offer')->where('Type','J')->paginate(5);
        return view('company.allOffers')->with('array', $offers);
    }

    public function internships(Request $request)
    {
        $offers = DB::table('offer')->where('Type','I')->paginate(5);
        return view('company.allOffers')->with('array', $offers);

    }
	
	public function allCompanies(Request $request)
    {
        $companies = DB::table('company')->orderBy('Username')->paginate(3);
        return view('company.allCompanies', ['companies' => $companies]);
    }

    public function deleteOffers(Request $request)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $username = $_SESSION['Username'];
        $offers = DB::table('offer')->where('Username', $username)->get();

        return view('company.deleteOffers', ['offers' => $offers]);
    }

    public function deleteOff(Request $request)
	{
        $data = $request['todeact'];
        if ($data) {
            foreach ($data as $offer) {
                DB::table('offer')->where('Id', $offer)->delete();
            }
        }
        return redirect()->back();
    }
}
