<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Question;
use App\User;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{

    public function addQuestion()
    {
        return view('tests.addQuestion');
    }

    public function add(Request $request)
    {
        $questionAndAnswers=$request->question;

        $questionType="";
        if (isset($_POST['confirm']))
        {
            if (isset($_POST['answerType']))
            {
                $questionType = $_POST['answerType'];

            }
        }

        $questionAnswer=$request->answers;

        $question=new Question();
        $question->QText=$questionAndAnswers;
        $question->Answer=$questionAnswer;

        if($questionType==1)
        {
            $question->QType='o';/*one*/
        }
        else if($questionType==2)
        {
            $question->QType='m';/*multiple*/
        }
        else
        {
            $question->QType='t';/*text*/
        }

        $question->save();

        return redirect()->back()->withErrors(['success' => 'Pitalica je uspesno dodata']);

    }

    public function showTest()
    {
        $q1=null;
        $q2=null;
        $q3=null;
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $questions = DB::table('question')->where('QType', '=', 'o')->get();;
        $randomQ=rand(0,$questions->count()-1);
        $count=0;
        foreach($questions as $q )
        {
            if($count==$randomQ)
            {
                $q1=$q;
                $_SESSION['q1id']=$q->Id;
                break;
            }
            $count++;
        }

        $questions = DB::table('question')->where('QType', '=', 'm')->get();;
        $randomQ=rand(0,$questions->count()-1);
        $count=0;
        foreach($questions as $q )
        {
            if($count==$randomQ)
            {
                $q2=$q;
                $_SESSION['q2id']=$q->Id;
                break;
            }
            $count++;
        }

        $questions = DB::table('question')->where('QType', '=', 't')->get();;
        $randomQ=rand(0,$questions->count()-1);
        $count=0;
        foreach($questions as $q )
        {
            if($count==$randomQ)
            {
                $q3=$q;
                $_SESSION['q3id']=$q->Id;
                break;
            }
            $count++;
        }


        return view('tests.test',['q1'=>$q1,'q2'=>$q2,'q3'=>$q3]);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function testSubmition(Request $request)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $points=0;

        $answer1=nullValue();
        $answer2=nullValue();
        $answer3=nullValue();


                $answer1 = $request['answer'];
                if (isset($_SESSION['q1id']))
                {

                    $id=$_SESSION['q1id'];
                    $question=DB::table('question')
                        ->where('Id', $id)
                        ->first();
                    $answer=$question->Answer;
                  //  echo 'expected1',' ',$answer,PHP_EOL;
                    if($answer==$answer1)
                        $points++;
                    unset($_SESSION['q1id']);
                }


                $answer2 = $request['answers'];
               /*foreach($answer2 as $a)
                echo 'answer2',' ',$a,PHP_EOL;*/
                $answer=nullValue();
                if (isset($_SESSION['q2id']))
                {

                    $id=$_SESSION['q2id'];
                    $question=DB::table('question')
                        ->where('Id', $id)
                        ->first();
                    $answer=$question->Answer;
                  //  echo 'expected2',' ',$answer,PHP_EOL;
                    unset($_SESSION['q2id']);
                }
                $arr=$answer2;
                $expected=explode(PHP_EOL,$answer);
               /* echo 'arr',' ',count($arr),' ';
                foreach($arr as $a)
                     echo $a,' ';
                echo 'expected',' ',count($expected),' ';
                foreach($expected as $e)
                    echo $e,' ';
                */
                $ok=1;
                if(count($expected)==count($arr))
                {
                    $count=0;
                    foreach($arr as $a)
                    {
                        if($expected[$count]!=$a)
                            $ok=0;
                        $count++;
                    }

                    if($ok==1)
                        $points++;
                }

       // echo $points;


                $answer3=$request['txtanswer'];
                //echo 'answer3',' ',$answer3,PHP_EOL;
                if (isset($_SESSION['q3id']))
                {

                    $id=$_SESSION['q3id'];
                    $question=DB::table('question')
                        ->where('Id', $id)
                        ->first();
                    $answer=$question->Answer;
                 //   echo 'expected3',$answer, PHP_EOL;
                    if($answer==$answer3)
                        $points++;
                    unset($_SESSION['q3id']);
                }


       // echo $points;

        $user=Auth::user();
        $user->Points+=$points;
        $user->save();
		
		$count=1;
        $userList = User::orderBy('Points','desc')->get();
        foreach($userList as $user)
        {
            $user->Rank=$count;
            $user->save();
            $count++;
        }
		
		$err="<script type='text/javascript'>alert('Osvojeni broj poena je: ".$points."');</script>";
        return redirect()->back()->withErrors(['success' => $err]);
    }
}
