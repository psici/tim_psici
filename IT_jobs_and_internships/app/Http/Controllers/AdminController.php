<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
   public function changeToAdmin()
    {
        $userList = DB::table('users')->orderBy('Username')->paginate(10);

        return view('admin.changeUserToAdmin', ['userList' => $userList]);
    }

    public function getUsersBySearch(/*$search*//*Request $request*/)
    {

        $search=$_GET['theSearch'];
        //echo $search;
        //$search=$request['search'];
       // $search=$_POST['theSearch'];
        if ($search)
        {
            $users = User::orderBy('Username')
                ->where('Username', 'LIKE', '%' . $search . '%')
                ->paginate(10);
        }
        else
        {
            $users = DB::table('users')->orderBy('Username')->paginate(10);
        }
        //if($request->ajax())
           // echo $request['search'];
       // else
         //   echo 'nije ajax';
        //return view('admin.changeUserToAdmin', ['userList' => $users]);
        $view=view('admin.content',['userList'=>$users])->render();
        return response($view);
    }

    public function getUsersPagination()
    {
        $search=$_GET['theSearch'];
        //echo $search;
        //$search=$request['search'];
        // $search=$_POST['theSearch'];
        if ($search)
        {
            $users = User::orderBy('Username')
                ->where('Username', 'LIKE', '%' . $search . '%')
                ->paginate(10);
        }
        else
        {
            $users = DB::table('users')->orderBy('Username')->paginate(10);
        }
        //if($request->ajax())
        // echo $request['search'];
        // else
        //   echo 'nije ajax';
        //return view('admin.changeUserToAdmin', ['userList' => $users]);
        $view=view('admin.content',['userList'=>$users])->render();
        return response($view);
    }

    public function postChangeToAdmin(Request $request)
    {
        $data = $request['toadmin'];
        if ($data) {
            foreach ($data as $username) {
                DB::table('users')
                    ->where('Username', $username)
                    ->update(['IsAdmin' => 1]);
            }
        }
        return redirect()->back();
    }


    public function deactivateUserProfiles(Request $request)
    {
        $type = $request->userType;
        if ($type == "user") {
            if ($request->search) {
                $userList = User::orderBy('Username')
                    ->where('Username', 'LIKE', '%' . $request->search . '%')
                    ->paginate(10);
            } else {
                $userList = User::orderBy('Username')->paginate(10);
            }
        } else {
            if ($request->search) {
                $userList = Company::orderBy('Username')
                    ->where('Username', 'LIKE', '%' . $request->search . '%')
                    ->paginate(10);
            } else {
                $userList = Company::orderBy('Username')->paginate(10);
            }
        }
        return view('admin.deleteUserProfiles', ['userList' => $userList, 'type' => $type]);
    }

    public function postDeactivateUserProfiles(Request $request)
    {
        $data = $request['todeact'];
        if ($data) {
            foreach ($data as $username) {
                DB::table('users')->where('Username', $username)->delete();
                DB::table('company')->where('Username', $username)->delete();
            }
        }
        return redirect()->back();
    }
	
	
    public function deleteComments(Request $request)
    {
        $comments = DB::table('comment')->orderBy('Id')->paginate(5);
        return view('admin.allComments', ['comments' => $comments]);
    }

    public function deleteCom(Request $request){
        $data = $request['todeact'];
        if ($data) {
            foreach ($data as $Id) {
                DB::table('comment')->where('Id', $Id)->delete();
            }
        }
        return redirect()->back();
    }
}
