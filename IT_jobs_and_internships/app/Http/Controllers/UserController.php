<?php

namespace App\Http\Controllers;

use Illuminate\Http\File;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use \Crypt;
use Illuminate\Support\Facades\Input;


class UserController extends Controller
{

    public function SignIn(Request $request)
    {

        $user_type="";
        if(isset($_POST['login']))
        {
            $user_type=$_POST['selected_text'];
        }

        if(strcmp($user_type,"Korisnik")==0)
        {
            echo "user";
            $result=app('App\Http\Controllers\UserController')->UserSignIn($request);
            if($result==1)
            {
                return redirect()->route('userprofile');
            }
            else
            {
                return redirect()->back()->withInput()->withErrors(['success' => 'Korisnicko ime ili lozinka su pogresni. Pokusajte opet']);
                /*return redirect()->back();*/
            }

        }
        else
        {
            $result=app('App\Http\Controllers\CompanyController')->SignIn($request);
            if($result==1)
            {
                return redirect('company_profile/' . $request->uname);
            }
            else
            {
                return redirect()->back()->withInput()->withErrors(['success' => 'Korisnicko ime ili lozinka su pogresni. Pokusajte opet']);
                /*return redirect()->back();*/
            }

        }
    }


    public function UserSignIn(Request $request)
    {
        $user = DB::table('users')
            ->where('Username',$request->uname)
            ->first();

        if (is_null($user)) {
            return 0;
        }
        else {
            $Administrator = DB::table('users')->where('Username',$request->uname)->first();
            $isAdministrator = $Administrator->IsAdmin;
            echo Crypt::decryptString($user->Pass);
            if ($request->password == Crypt::decryptString($user->Pass)) {
                if (session_status() == PHP_SESSION_NONE) {
                    session_start();
                }
                $_SESSION['Username'] = $request->uname;
                $_SESSION['Greska'] = '';
                $_SESSION['Header'] = 'navbar';
                if ($isAdministrator==1)
                    $_SESSION['Header']='admin_header';
                $nuser = new User();
                $nuser->Username = $user->Username;
                $nuser->FirstName =$user->FirstName;
                $nuser->LastName = $user->LastName;
                $nuser->Pass = $user->Pass;
                $nuser->Email = $user->Email;
                $nuser->Birthdate = $user->Birthdate;
                $nuser->Gender = $user->Gender;
                $nuser->Points = $user->Points;
                $nuser->IsAdmin = $user->IsAdmin;
                $nuser->Site=$user->Site;
                $nuser->Faculty=$user->Faculty;
                $nuser->Department=$user->Department;
                $nuser->Experience=$user->Experience;
                $nuser->Skills=$user->Skills;
                $nuser->Awards=$user->Awards;
                Auth::login($nuser);
                return 1;
            } else
                return 0;
        }
    }
    public function signUp()
    {
        return view('user.userSignUp');
    }

    public function updateUser()
    {
        return view('user.updateUser', ['userInfo' => Auth::user()]);
    }

    public function userProfile()
    {
        return view('user.userProfile', ['userInfo' => Auth::user()]);
    }

    public function postSignUp(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users|max:30',
            'firstname' => 'required|max:30',
            'lastname' => 'required|max:30',
            'newpass' => 'required|min:6|alpha_dash',
            'newpassagain' => 'required_with:newpass|same:newpass',
            'email' => 'required|email|max:100',
            'cv' => 'file'
        ]);

        $username = $request['username'];
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $pass = Crypt::encryptString($request['newpass']);
        $email = $request['email'];
        $birthdate = $request['birthdate'];
        $gender = $request['gender'];

        $user = new User();

        if (Input::hasFile('cv')) {
            Input::file('cv')->move('uploads', $request['username'] . '_cv.pdf');
            $path_to_cv = "uploads/" . $request['username'] . '_cv.pdf';
            $user->PathCV = $path_to_cv;
        }

        $user->Username = $username;
        $user->FirstName = $firstname;
        $user->LastName = $lastname;
        $user->Pass = $pass;
        $user->Email = $email;
        $user->Birthdate = $birthdate;
        $user->Gender = $gender;
        $user->Points = 0;
        $user->IsAdmin = 0;

        $user->save();

        $Administrator = DB::table('users')->where('Username',$username)->first();
        $isAdministrator = $Administrator->IsAdmin;

        Auth::login($user);

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['Username'] = $request['username'];
        $_SESSION['Greska'] = "";
        $_SESSION['Header'] = 'navbar';
        if ($isAdministrator==1)
            $_SESSION['Header']='admin_header';
        return redirect()->route('updateUser');
    }


    public function postUpdateUser(Request $request)
    {
        $user = Auth::user();
        $oldUsername = $user->Username;
        $oldPass = $user->Pass;

        if ($request['oldpass'] != '' && $request['oldpass'] != Crypt::decryptString($oldPass)) {
            return redirect()->back()->withInput()->withErrors(array('oldpass' => 'Inputed old pass is not same as current.'));
        }

        $username = $request['username'];
        $existingUser =  DB::table('Users')
                    ->where('Username', $username)
                    ->where('Username', '<>', $oldUsername)
                    ->get();
        if (count($existingUser) != 0) {
            return redirect()->back()->withInput()->withErrors(array('username' => 'This username is already taken.'));
        }

        //za sada ovako
        $this->validate($request, [
            'username' => 'max:30',
            'firstname' => 'max:30',
            'lastname' => 'max:30',
            'oldpass' => 'required_with:newpass,newpassagain|nullable',
            'newpass' => 'required_with:newpassagain|min:6|alpha_dash|nullable',
            'newpassagain' => 'required_with:newpass|same:newpass',
            'email' => 'nullable|email|max:100',
            'cv' => 'file',
            'site' => 'max:50',
            'faculty' => 'max:50',
            'department' => 'max:50',
            'experience' => 'max:255',
            'skills' => 'max:255',
            'awards' => 'max:255',
        ]);


        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        if ($request['newpass']!= '') {
            $pass = Crypt::encryptString($request['newpass']);
        } else {
            $pass = '';
        }
        $email = $request['email'];
        $faculty = $request['faculty'];
        $department = $request['department'];
        $experience = $request['experience'];
        $skills = $request['skills'];
        $awards = $request['awards'];
        $birthdate = $request['birthdate'];
        $gender = $request['gender'];
        $site = $request['site'];

        if ($username == '') {
            $username = $oldUsername;
        }
        if (Input::hasFile('cv')) {
            Input::file('cv')->move('uploads', $request['username'] . '_cv.pdf');
            $path_to_cv = "uploads/" . $request['username'] . '_cv.pdf';
            $user->PathCV = $path_to_cv;
        }

        if (Input::hasFile('picture')) {
            Input::file('picture')->move('uploads', $request['username'] . '_picture.png');
            $path_to_picture = "uploads/" . $request['username'] . '_picture.png';
            $user->PathPicture = $path_to_picture;
        } else if (!file_exists ( "uploads/" . $request['username'] . '_picture.png')) {
            $user->PathPicture = "images/defaultUserPic.png";
        }

        //ova polja ne smeju da budu prazna
        if ($username != '') {
            $user->Username = $username;
        }
        if ($firstname != '') {
            $user->FirstName = $firstname;
        }
        if ($lastname != '') {
            $user->Lastname = $lastname;
        }
        if ($pass != '') {
            $user->Pass = $pass;
        }
        if ($email != '') {
            $user->Email = $email;
        }
        $user->Faculty = $faculty;
        $user->Department = $department;
        $user->Experience = $experience;
        $user->Skills = $skills;
        $user->Awards = $awards;
        $user->Birthdate = $birthdate;
        $user->Gender = $gender;
        $user->Site = $site;

        $user->save();
        return redirect()->route('userprofile');
    }

    public function deactivate()
    {
        $username = Auth::user()->Username;
        Auth::logout();
        User::destroy($username);
        return redirect()->route('home');
    }

    public function cvDownload($fileName)
    {
        if (file_exists ( "uploads/" . $fileName)) {
            $file = "uploads/" . $fileName;
            return response()->download($file);
        }
    }

    public function logoutUser()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['Username']="";
        session_destroy();
        Auth::logout();
        return redirect()->route('home');
    }
	
	public function showRankList()
    {
        
        $userList = User::orderBy('Points','desc')->paginate(10);
        return view('user.rankList', ['userList' => $userList]);
    }
	
	public function allUsers(Request $request)
    {
        $users = DB::table('users')->orderBy("Username")->paginate(5);
        return view('user.allUsers', ['users' => $users]);
    }

}

