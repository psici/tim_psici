<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{


    private $timestamp = false;
    protected $table = "company";
    protected $id  = "Username";


    var $NumberOfGrades;
    var $SumOfGrades;


    public function __construct(){
        $this->NumberOfGrades = 0;
        $this->SumOfGrades = 0;
        $this->AverageGrade = 0;
    }

    public function calculateGrade(){
        if ($this->NumberOfGrades == 0)
            $this->AverageGrade = 0;
        else
            $this->AverageGrade = $this->SumOfGrades / $this->NumberOfGrades;
    }

    public function addGrade($grade){
        $this->NumberOfGrades++;
        $this->SumOfGrades += $grade;
        $this->calculateGrade();
    }
}
