<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;
use  App\Company;


class CompanyController extends BaseController {

    public function index() {
        echo 'Nothing here yet.';
    }

    public function comment(){

        DB::table('comment')->insert(
            array('UserName' => 'Korisnik - 20.5.2017.', 'Company' => Input::get("firma"),'Description' => Input::get("komentar"))
        );

        $firma = DB::table('company')->where('Username',Input::get("firma"))->first();

        return redirect('profilfirma/'.$firma->Username.'');
    }

    public function profile($id){
        echo 'redirektovani';


        $firma = DB::table('company')->where('Username',$id.'')->first();
        $komentari = DB::table('comment')->where('Company',$id.'')->get();


        $data['Username'] = $firma->Username;
        $data['Password'] = $firma->Password;
        $data['E_mail']   = $firma->E_mail;
        $data['Sector']   = $firma->Sector;
        $data['Description'] = $firma->Description;
        $data['Telephone']   = Input::Get("telefon");
        $data['Site']     =  $firma->Site;
        $data['NumOfEmployees'] = $firma->NumOfEmployees;
        $data['FoundationYear'] = $firma->FoundationYear;
        $data['FieldOfWork'] = $firma->FieldOfWork;
        $data['HeadQuarter'] = $firma->Headquarter;

        $path_to_picture = "images/default_company.png";
        $data['Picture'] = $path_to_picture;

        $data['Komentari'] = $komentari;



        return view('profilfirma',$data);

    }


    public function edit() {

        $path_to_picture = "images/default_company.png";

        if (Input::hasFile('slika')){
            Input::file('slika')->move('uploads',Input::get("ime").'-slika.png');
            $path_to_picture = "uploads/".Input::get("ime").'-slika.png';
        }

        DB::table('telephone_company')
            ->where('Username',Input::get("OldUsername"))
            ->where('Telephone',Input::Get("OldTelephone"))
            ->limit(1)
            ->update(array('Username'=>Input::get("ime"),
                           'Telephone'=>Input::get("telefon")));


        DB::table('company')
           -> where('Username',Input::Get("OldUsername"))
            -> limit(1)
            -> update(array('Username'=>Input::Get("ime"),
                            'Password'=>Input::get("lozinka"),
                            'NumOfEmployees'=>Input::get("broj_zaposlenih"),
                            'FoundationYear'=>Input::get("godina_osnivanja"),
                            'FieldOfWork'=>Input::get("specijalnosti"),
                            'E_mail'=>Input::get("email"),
                            'HeadQuarter'=>Input::get("sediste"),
                            'Sector'=>Input::get("sektor"),
                            'Site'=>Input::get("sajt"),
                            'Description'=>Input::get("opis")));


        $data['Username'] = Input::Get("ime");
        $data['Password'] = Input::Get("lozinka");
        $data['E_mail']   = Input::Get("email");
        $data['Sector']   = Input::Get("sektor");
        $data['Description'] = Input::Get("opis");
        $data['Telephone']   = Input::Get("telefon");
        $data['Site']     =  Input::get("sajt");
        $data['NumOfEmployees'] = Input::get("broj_zaposlenih");
        $data['FoundationYear'] = Input::get("godina_osnivanja");
        $data['FieldOfWork'] = Input::get("specijalnosti");
        $data['HeadQuarter'] = Input::get("sediste");
        $data['Picture'] = $path_to_picture;


        $komentari = DB::table('comment')->where('Company',Input::Get("ime"))->get();

        $data['Komentari'] = $komentari;

        return view('profilfirma',$data);
    }


    public function store() {
        $company = new Company;

        $company->Username = Input::get("fkime");
        $company->Password = Input::get("flozinka");
        $company->E_mail   = Input::Get("femail");
        $company->Sector = Input::Get("sektor");
        $company->Description = Input::Get("fopis");
        $company->Site = Input::Get("fsajt");

        $company->NumOfEmployees = 0;
        $company->FoundationYear = 2017;
        $company->FieldOfWork = "No informations yet";
        $company->HeadQuarter = "No informations yet";


        $company->save();

        $company->Telephone=Input::Get("ftelefon");

        DB::table('telephone_company')->insert((array('Telephone' => $company->Telephone , 'Username' => $company->Username)));


        $data['Username'] = $company->Username;
        $data['Password'] = $company->Password;
        $data['E_mail']   = $company->E_mail;
        $data['Sector']   = $company->Sector;
        $data['Description'] = $company->Description;
        $data['Telephone']   = $company->Telephone;
        $data['Site']     =  $company->Site;
        $data['NumOfEmployees'] = $company->NumOfEmployees;
        $data['FoundationYear'] = $company->FoundationYear;
        $data['FieldOfWork'] = $company->FieldOfWork;
        $data['HeadQuarter'] = $company->HeadQuarter;

        return view('azuriranjefirma',$data);
    }
}