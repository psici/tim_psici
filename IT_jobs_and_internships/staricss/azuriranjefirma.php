<html> 
<head>
	<meta charset="UTF-8">
	<title> Azuriranje informacija o firmi </title>
	<link rel="stylesheet" href="<?php echo asset('css/style.css')?>" type="text/css">
</head>
<body> 
	<div id="page">

	<?php
		require_once('meni_korisnik.php');
	?>


	<div class="body">
					<br/>
					<h3 align="center">
						<font color = "#800000"> 
							Informacije o firmi 
						</font>
					</h3>
					<br/>
					<form action="UpdateFirm" method="post" id="message" enctype="multipart/form-data" class="section">
                        <input type="hidden" name="OldUsername" value=<?php echo $Username ?> lenght="30"/>
                        <input type="hidden" name="OldTelephone" value=<?php echo $Telephone ?> length="30"/>
					 <table name="azuriranjefirme" width="60%"> 
					 	<tr>
					 		<td align="left">
					 			<label align="left"><font color="black" align="left"> Ime firme : </font></label>
					 		</td>
					 		<td>
					 			<input type="text" name="ime" value=<?php  echo $Username ?> length="30"/>
					 		</td>
					 		<td width="3%">
					 			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					 		</td>
					 		<td>
					 			<label><font color="black"> Broj zaposlenih </font></label>
					 		</td>
					 		<td>
					 			<input type="text" name="broj_zaposlenih" value=<?php echo $NumOfEmployees ?> length="20"/>
					 		</td>
					 	</tr>
					 		<td>
					 			<label><font color="black"> Lozinka : </font></label>
					 		</td>
					 		<td>
					 			<input type="password" name="lozinka" value=<?php echo $Password ?> length="30"/>
					 		</td>
					 		<td width="3%">
					 		</td>
					 		<td>
					 			<label><font color="black"> Godina osnivanja </font></label>
					 		</td>
					 		<td>
					 			<input type="text" name="godina_osnivanja" value=<?php echo $FoundationYear ?> length="20"/>
					 		</td>
					 	<tr>
					 		<td>
					 			<label><font color="black"> Lozinka opet : </font></label>
					 		</td>
					 		<td>
					 			<input type="password" name="lozinka_opet" value=<?php echo $Password ?> length="20"/>
					 		</td>
					 		<td width="3%">
					 		</td>
					 		<td>
					 			<label><font color="black"> Specijalnosti </font></label>
					 		</td>
					 		<td>
                                <?php echo "<input type='text' name='specijalnosti' value='$FieldOfWork' length='40'/>" ?>
					 		</td>
					 	<tr/>
					 	<tr>
					 		<td>
					 			<label align="left"><font color="black"> Email  : </font></label>
					 		</td>
					 		<td>
					 			<input type="text" name="email" value=<?php echo $E_mail ?> length="30"/>
					 		</td>
					 		<td width="3%">
					 		</td>
					 		<td align="left">
					 			<label><font color="black"> Sediste </font></label>
					 		</td>
					 		<td>
					 			<?php echo "<input type='text' name='sediste' value='$HeadQuarter' length='40'/>" ?>
					 		</td>
					 	<tr/>
					 	<tr>
					 		<td>
					 			<label align="left"><font color="black"> Telefon: </font></label>
					 		</td>
					 		<td>
					 			<input type="text" name="telefon" value=<?php echo stripslashes($Telephone)?> length="30"/>
					 		</td>
					 		<td width="3%"> 
					 		</td>
					 		<td> 
					 			<label align="left"><font color="black"> Sektor :</font></label>
					 		</td>
					 		<td>
					 			<select name="sektor">
					 				<option name="it" value="IT"> IT </option>
					 				<option name="telekomunikacije" value="Telekomunikacije"> Telekomunikacije </option>
					 				<option name="elektronika" value="Elektronika"> Elektronika </option>
					 				<option name="automatika" value="Automatika"> Automatika </option>
					 			</select>
					 		</td>
					 	</tr>
					 	<tr>
					 		<td>
					 			<label align="left"><font color="black"> Web Sajt: </font></label>
					 		</td>
					 		<td>
					 			<input type="text" name="sajt" value=<?php echo $Site ?> length="30"/>
					 		</td>
					 		<td width="3%"> 
					 		</td>
					 		<td > 
					 			<label align="left"><font color="black"> Slika :</font></label>
					 		</td>
					 		<td>
					 			<input type="file" name="slika"/>
					 		</td>
					 	</tr>
					 	<tr>
					 		<td> 
					 			<label align="left"><font color="black"> Opis :</font></label>
					 		</td>
					 		<td>
                                <textarea name='opis' rows=2 cols=50> <?php echo $Description; ?> </textarea>
					 		</td>
					 		<td width="3%">
					 		</td>
					 		<td colspan="2"> 
					 			<input type="submit" name="potvrdi" value="Potvrdi"/>
					 			
					 			<input type="reset" name="ponisiti" value="Ponisti"/>
					 		</td>
					 	</tr>

					 </table>
				   </form>
	</div>



	<?php
		require_once('footer.php'); 
	?>

	</div>
</body>
</html>
