<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    //Odvojicu komentarima gde su cije rute da se ne pogubimo dok trazimo svoje,pa cemo na kraju da sklonimo komentare :D
    // Dragana

//Stavljajte sve rute u okviru ovoga

Route::group(['middleware' => ['web']], function() {

    Route::get('/', function () {
        return view('home');
    }) -> name ('home');

    Route::get('/registracija_korisnika', 'UserController@signUp') -> name('registerUser');

    Route::get('/azuriranje_korisnika', 'UserController@updateUser') -> name('updateUser')->middleware('user');

    Route::get('/profil_korisnika/', [
        'uses' => 'UserController@userProfile',
        'as' => 'userprofile'
    ])->middleware('user');


    Route::post('/registracija', [
        'uses' => 'UserController@postSignUp',
        'as' => 'usersignup'
    ]);

    Route::post('/azuriranje', [
        'uses' => 'UserController@postUpdateUser',
        'as' => 'userupdate'
    ])->middleware('user');


    Route::get('/userImage/{imageName}', [
        'uses' => 'UserController@getUserImage',
        'as' => 'profile.image'
    ]);

    Route::get('/deactivateUser', [
        'uses' => 'UserController@deactivate',
        'as' => 'profile.deactivate'
    ])->middleware('user');

    Route::get('/cvDownload/{fileName}', [
        'uses' => 'UserController@cvDownload',
        'as' => 'profile.cvdownload'
    ]);


    Route::get('/promenaUAdmina', [
        'uses' => 'AdminController@changeToAdmin',
        'as' => 'changeToAdmin'
    ])->middleware('admin');

    Route::post('/promenaKorisnikaUAdmine', [
        'uses' => 'AdminController@postChangeToAdmin',
        'as' => 'changeUserToAdmin'
    ])->middleware('admin');

    Route::get('/brisanjeKorisnika', [
        'uses' => 'AdminController@deactivateUserProfiles',
        'as' => 'deactivateProfiles'
    ])->middleware('admin');

    Route::post('/brisanjeKorisnikaPost', [
        'uses' => 'AdminController@postDeactivateUserProfiles',
        'as' => 'deactivateUserProfiles'
    ])->middleware('admin');
    //Milica
	
	Route::get('/nadjiKorisnike',[
        'uses'=>'AdminController@getUsersBySearch',
        'as'=>'findUsers'
    ]);

	 Route::get('/nadjiKorisnikePaginacija',[
         'uses'=>'AdminController@getUsersPagination',
         'as'=>'getUsersPagination'
     ]);
	
    Route::post('/prijava', [
        'uses' => 'UserController@SignIn',
        'as' => 'signin'
    ]);

    Route::post('/prijava_firma', [
        'uses' => 'CompanyController@SignIn',
        'as'=>'company_signin'
        ]);

    Route::post('/prijava_korisnik', [
        'uses' => 'UserController@UserSignIn',
        'as'=>'user_signin'
    ]);

    Route::get('/odjava_korisnik',[
        'uses'=>'UserController@logoutUser',
        'as'=>'logoutUser'
    ])->middleware('user');

    Route::get('/odjava_firma',[
        'uses'=>'CompanyController@logoutCompany',
        'as'=>'logoutCompany'
    ])->middleware('company');
	
	 Route::get('/dodaj_pitalice',[
          'uses'=>'QuestionController@addQuestion',
          'as'=>'addQuestion'
    ])->middleware('admin');

    Route::post('/dodaj_pitanje',[
        'uses'=>'QuestionController@add',
        'as'=>'add'
    ])->middleware('admin');
	
	Route::get('/izrada_testa',[
        'uses'=>'QuestionController@showTest',
        'as'=>'showTest'
    ])->middleware('user');

    Route::post('/provera_odgovora',[
        'uses'=>'QuestionController@testSubmition',
        'as'=>'checkAnswers'
    ])->middleware('user');

    Route::get('/prikaz_rang_liste',[
        'uses'=>'UserController@showRankList',
        'as'=>'rankList'
    ]);

   //Ivana

    Route::get('/Poslovi',[
        'uses'=>'CompanyController@jobs',
        'as'=>'jobs'
    ]);

    Route::get('/Prakse',[
        'uses'=>'CompanyController@internships',
        'as'=>'internships'
    ]);

    Route::get('/Firme',[
        'uses'=>'CompanyController@allCompanies',
        'as'=>'allCompanies'
    ]);

    Route::get('/Korisnici',[
        'uses'=>'UserController@allUsers',
        'as'=>'allUsers'
    ]);

    Route::get('/brisanjeKomentara', [
        'uses' => 'AdminController@deleteComments',
        'as' => 'deleteComments'
    ])->middleware('admin');

    Route::get('/brisanje', [
        'uses' => 'AdminController@deleteCom',
        'as' => 'deleteCom'
    ])->middleware('admin');
	
	
    Route::get('/brisanjePonuda', [
        'uses' => 'CompanyController@deleteOffers',
        'as' => 'deleteOffers'
    ])->middleware('company');

    Route::post('/brisanjePon', [
        'uses' => 'CompanyController@deleteOff',
        'as' => 'deleteOff'
    ])->middleware('company');

	
	 //Zlatan

    Route::get("/company_registration",'CompanyController@registration')->name('registerCompany');

    Route::get("/updateCompany",'CompanyController@edit')->middleware('company');

    Route::get("/registerCompany",'CompanyController@validateUpdate')->middleware('company');

    Route::post("/registerCompany",'CompanyController@create');

    Route::post("/updateCompany",'CompanyController@edit')->middleware('company');

    Route::post("/AddComment",'CompanyController@comment')->middleware('user');

    Route::get('company_profile/{id}',['uses'=>'CompanyController@profile','as'=>'company_profile']);

    Route::get('company_offers/{id}',['uses'=>'CompanyController@offers','as'=>'company_offers']);

    Route::post("company_profile/AddComment",'CompanyController@comment');

    Route::post("company_profile/ThumbUp",'CompanyController@ThumbUp');

    Route::post("company_profile/ThumbDown",'CompanyController@ThumbDown');

    Route::post("company_profile/RateFirm",'CompanyController@RateFirm');

    Route::post("/RateFirm",'CompanyController@RateFirm');

    Route::post("/ThumbUp",'CompanyController@ThumbUp');

    Route::post("/ThumbDown",'CompanyController@ThumbDown');

    Route::get("/createAccount",['as'=>'createAccount','uses' => 'CompanyController@EditInfo']);

    Route::get("/AddOffer",['as'=>'AddOffer','uses' => 'CompanyController@AddOffer'])->middleware('company');

    Route::post("/OfferAdded",'CompanyController@OfferAdded')->middleware('company');

    Route::get('/deactivateCompany', ['uses' => 'CompanyController@deactivate', 'as' => 'company.deactivate'])->middleware('company');

});





